/* See the LICENSE file at the top-level directory of this distribution. */

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cuda_runtime_api.h>
#include "harp_beam.h"
#include "read_write_hdf5.h"


void evaluate_eeps_cpu(
    const float freq,
    const int max_order,
    const int num_dir,
    const int num_ant,
    const int num_mbf,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    const float* const RESTRICT ant_x,
    const float* const RESTRICT ant_y,
    const float* const RESTRICT ant_z,
    const float2* const RESTRICT alpha_te,
    const float2* const RESTRICT alpha_tm,
    const float2* const RESTRICT coeffs,
    float2* RESTRICT eeps_theta,
    float2* RESTRICT eeps_phi)
{
    float2* pth = (float2*)malloc(num_mbf * num_dir * sizeof(float2));
    float2* pph = (float2*)malloc(num_mbf * num_dir * sizeof(float2));

    /*
     * STEP 2: Evaluate the MBF patterns.
     */
    clock_t begin = clock();
    float* poly = (float*)malloc(num_dir * max_order * (max_order + 1) * sizeof(float)); // Legendre polynomials
    float2* ee = (float2*)malloc(num_dir * (2 * max_order + 1) * sizeof(float2)); // exp(jmphi)
    float2* qq = (float2*)malloc(num_dir * max_order * (2 * max_order + 1) * sizeof(float2)); // qq and dd values appearing in sw decomposition
    float2* dd = (float2*)malloc(num_dir * max_order * (2 * max_order + 1) * sizeof(float2));
    harp_precompute_smodes_mbf_float(max_order, num_dir, theta, phi, poly, ee, qq, dd); // precomputation of smodes
    harp_reconstruct_smodes_mbf_float(max_order, num_dir, num_mbf, alpha_te, alpha_tm, qq, dd, pth, pph); // reconstruction of MBF patterns from their sw coefficients
    free(ee);
    free(poly);
    free(qq);
    free(dd);
    clock_t end = clock();
    float time_step2 = (float)(end - begin) / CLOCKS_PER_SEC;

    /*
     * STEP 3: Assemble each beam from MBF patterns and MBF coefficients.
     */
    begin = clock();
    float2* phase_fac = (float2*)malloc(num_dir * num_ant * sizeof(float2)); // phasefactor = exp(jkx x_ant + ky y_ant)
    harp_evaluate_phase_fac_float(num_dir, num_ant, freq, theta, phi, ant_x, ant_y, ant_z, phase_fac); // precompute phase factor
    harp_assemble_element_beams_float(num_mbf, num_ant, num_dir, coeffs, pth, pph, phase_fac, 1, 0, 0, eeps_theta, eeps_phi);
    free(phase_fac);
    end = clock();
    float time_step3 = (float)(end - begin) / CLOCKS_PER_SEC;

    free(pth);
    free(pph);

    printf("eval mbf patterns : %f secs \n", time_step2);
    printf("assemble element beams : %f secs \n", time_step3);
}

void permute_harp_coeffs(
    const int num_ant,
    const int num_mbf,
    const float2* const RESTRICT coeffs_in,
    float2*  coeffs_out)
{

    for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
    {
        for (int j_ant = 0; j_ant < num_ant; j_ant++)
        {
            int shift0 = j_ant * num_mbf + i_mbf;

            for (int i_ant = 0; i_ant < num_ant; i_ant++)
            {
                int shift = j_ant * num_ant + i_ant;
                int index_in = shift0 + i_ant * num_mbf * num_ant;
                int index_out = shift + i_mbf * num_ant * num_ant;

                coeffs_out[index_out] = coeffs_in[index_in];
            }
        }
    }
}

void evaluate_eeps_gpu(
    const float freq,
    const int max_order,
    const int num_dir,
    const int num_ant,
    const int num_mbf,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    const float* const RESTRICT ant_x,
    const float* const RESTRICT ant_y,
    const float* const RESTRICT ant_z,
    const float2* const RESTRICT alpha_te,
    const float2* const RESTRICT alpha_tm,
    float2*  coeffs,
    float2* RESTRICT eeps_theta,
    float2* RESTRICT eeps_phi)
{
    // Events to record computation times on the device.
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    float msecs = 0.0f;

    // Copy input data from host to device.
    float *d_theta, *d_phi, *d_ant_x, *d_ant_y, *d_ant_z;
    float2 *d_coeffs, *d_alpha_te, *d_alpha_tm, *d_eeps_theta, *d_eeps_phi;

    // 1) harp data.
    cudaMalloc((void**)&d_ant_x, num_ant * sizeof(float));
    cudaMalloc((void**)&d_ant_y, num_ant * sizeof(float));
    cudaMalloc((void**)&d_ant_z, num_ant * sizeof(float));
    cudaMalloc((void**)&d_coeffs, num_ant * num_ant * num_mbf * sizeof(float2));
    cudaMalloc((void**)&d_alpha_te, num_mbf * max_order * (2 * max_order + 1) * sizeof(float2));
    cudaMalloc((void**)&d_alpha_tm, num_mbf * max_order * (2 * max_order + 1) * sizeof(float2));
    cudaMemcpy(d_ant_x, ant_x, num_ant * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ant_y, ant_y, num_ant * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ant_z, ant_z, num_ant * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_coeffs, coeffs, num_ant * num_ant * num_mbf * sizeof(float2), cudaMemcpyHostToDevice);
    cudaMemcpy(d_alpha_te, alpha_te, num_mbf * max_order * (2 * max_order + 1) * sizeof(float2), cudaMemcpyHostToDevice);
    cudaMemcpy(d_alpha_tm, alpha_tm, num_mbf * max_order * (2 * max_order + 1) * sizeof(float2), cudaMemcpyHostToDevice);

    // 2) Beam data.
    cudaMalloc((void**)&d_theta, num_dir * sizeof(float));
    cudaMalloc((void**)&d_phi, num_dir * sizeof(float));
    cudaMemcpy(d_theta, theta, num_dir * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_phi, phi, num_dir * sizeof(float), cudaMemcpyHostToDevice);

    // 3) Allocate output beam values.
    cudaMalloc((void**)&d_eeps_theta, num_ant * num_dir * sizeof(float2));
    cudaMalloc((void**)&d_eeps_phi, num_ant * num_dir * sizeof(float2));

    // Allocate memory for intermediate output variables from step 1 and step 2
    float2 *d_pth, *d_pph;
    cudaMalloc((void**)&d_pth, num_mbf * num_dir * sizeof(float2));
    cudaMalloc((void**)&d_pph, num_mbf * num_dir * sizeof(float2));

    // STEP 2: Evaluate the MBF patterns.
    cudaEventRecord(start);
    float *d_poly;
    float2 *d_ee, *d_qq, *d_dd;
    cudaMalloc((void**)&d_poly, num_dir * max_order * (max_order + 1) * sizeof(float));
    cudaMalloc((void**)&d_ee, num_dir * (2 * max_order + 1) * sizeof(float2));
    cudaMalloc((void**)&d_qq, num_dir * max_order * (2 * max_order + 1) * sizeof(float2));
    cudaMalloc((void**)&d_dd, num_dir * max_order * (2 * max_order + 1) * sizeof(float2));
    harp_precompute_smodes_mbf_cuda_float(max_order, num_dir, d_theta, d_phi, d_poly, d_ee, d_qq, d_dd);
    harp_reconstruct_smodes_mbf_cuda_float(max_order, num_dir, num_mbf, d_alpha_te, d_alpha_tm, d_qq, d_dd, d_pth, d_pph);
    cudaFree(d_poly);
    cudaFree(d_ee);
    cudaFree(d_qq);
    cudaFree(d_dd);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&msecs, start, stop);
    float time_step2 = msecs * 1e-3;

    // STEP 3: Assemble each beam from MBF patterns and MBF coefficients.
    cudaEventRecord(start);
    float2 *d_phase_fac;
    cudaMalloc((void**)&d_phase_fac, num_dir * num_ant * sizeof(float2));
    harp_evaluate_phase_fac_cuda_float(num_dir, num_ant, freq, d_theta, d_phi, d_ant_x, d_ant_y, d_ant_z, d_phase_fac);
    harp_assemble_element_beams_cuda_float(num_mbf, num_ant, num_dir, d_coeffs, d_pth, d_pph, d_phase_fac, 1, 0, 0, d_eeps_theta, d_eeps_phi);

    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&msecs, start, stop);
    float time_step3 = msecs * 1e-3;

    cudaFree(d_pth);
    cudaFree(d_pph);

    // Copy beam values to host and free device memory.
    cudaMemcpy(eeps_theta, d_eeps_theta, num_ant * num_dir * sizeof(float2), cudaMemcpyDeviceToHost);
    cudaMemcpy(eeps_phi, d_eeps_phi, num_ant * num_dir * sizeof(float2), cudaMemcpyDeviceToHost);

    cudaFree(d_eeps_theta);
    cudaFree(d_eeps_phi);
    cudaFree(d_phi);
    cudaFree(d_theta);
    cudaFree(d_ant_x);
    cudaFree(d_ant_y);
    cudaFree(d_ant_z);
    cudaFree(d_alpha_tm);
    cudaFree(d_alpha_te);
    cudaFree(d_coeffs);

    printf("eval mbf patterns : %f secs \n", time_step2);
    printf("assemble element beams : %f secs \n", time_step3);
}


void evaluate_station_beam_cpu(
    const float freq,
    const int max_order,
    const int num_dir,
    const int num_ant,
    const int num_mbf,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    const float* const RESTRICT ant_x,
    const float* const RESTRICT ant_y,
    const float* const RESTRICT ant_z,
    const float2* const RESTRICT alpha_te,
    const float2* const RESTRICT alpha_tm,
    const float2* const RESTRICT coeffs,
    const float2* const RESTRICT weights,
    float2* RESTRICT beam_theta,
    float2* RESTRICT beam_phi)
{
    float2* beam_coeffs = (float2*)malloc(num_mbf * num_ant * sizeof(float2));
    float2* pth = (float2*)malloc(num_mbf * num_dir * sizeof(float2));
    float2* pph = (float2*)malloc(num_mbf * num_dir * sizeof(float2));

    /*
     * STEP 1: Evaluate the MBF coefficients of each beam.
     */

    clock_t begin = clock();
    harp_evaluate_beam_coeffs_float(num_mbf, num_ant, weights, coeffs, beam_coeffs);
    clock_t end = clock();
    float time_step1 = (float)(end - begin) / CLOCKS_PER_SEC;

    /*
     * STEP 2: Evaluate the MBF patterns.
     */
    begin = clock();
    float* poly = (float*)malloc(num_dir * max_order * (max_order + 1) * sizeof(float)); // Legendre polynomials
    float2* ee = (float2*)malloc(num_dir * (2 * max_order + 1) * sizeof(float2)); // exp(jmphi)
    float2* qq = (float2*)malloc(num_dir * max_order * (2 * max_order + 1) * sizeof(float2)); // qq and dd values appearing in sw decomposition
    float2* dd = (float2*)malloc(num_dir * max_order * (2 * max_order + 1) * sizeof(float2));
    harp_precompute_smodes_mbf_float(max_order, num_dir, theta, phi, poly, ee, qq, dd); // precomputation of smodes
    harp_reconstruct_smodes_mbf_float(max_order, num_dir, num_mbf, alpha_te, alpha_tm, qq, dd, pth, pph); // reconstruction of MBF patterns from their sw coefficients
    free(ee);
    free(poly);
    free(qq);
    free(dd);
    end = clock();
    float time_step2 = (float)(end - begin) / CLOCKS_PER_SEC;

    /*
     * STEP 3: Assemble each beam from MBF patterns and MBF coefficients.
     */
    begin = clock();
    float2* phase_fac = (float2*)malloc(num_dir * num_ant * sizeof(float2)); // phasefactor = exp(jkx x_ant + ky y_ant)
    harp_evaluate_phase_fac_float(num_dir, num_ant, freq, theta, phi, ant_x, ant_y, ant_z, phase_fac); // precompute phase factor
    harp_assemble_station_beam_float(num_mbf, num_ant, num_dir, beam_coeffs, pth, pph, phase_fac, 1, 0, 0, beam_theta, beam_phi);
    free(phase_fac);
    end = clock();
    float time_step3 = (float)(end - begin) / CLOCKS_PER_SEC;

    /*
     * Free outputs from step 1 and 2.
     *
     * Note: in case multiple station beams (different antenna positions
     * or weights) need to be evaluated at the same directions,
     * pth and pph can be kept in memory.
     */
    free(pth);
    free(pph);
    free(beam_coeffs);

    printf("eval beam coeffs : %f secs \n", time_step1);
    printf("eval mbf patterns : %f secs \n", time_step2);
    printf("assemble station beam : %f secs \n", time_step3);
}


void evaluate_station_beam_gpu(
    const float freq,
    const int max_order,
    const int num_dir,
    const int num_ant,
    const int num_mbf,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    const float* const RESTRICT ant_x,
    const float* const RESTRICT ant_y,
    const float* const RESTRICT ant_z,
    const float2* const RESTRICT alpha_te,
    const float2* const RESTRICT alpha_tm,
    const float2* const RESTRICT coeffs,
    const float2* const RESTRICT weights,
    float2* RESTRICT beam_theta,
    float2* RESTRICT beam_phi)
{
    // Events to record computation times on the device.
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    float msecs = 0.0f;

    /*
     * Copy input data from host to device.
     */
    float* d_theta, * d_phi, * d_ant_x, * d_ant_y, * d_ant_z;
    float2* d_coeffs, * d_weights, * d_alpha_te, * d_alpha_tm,
        * d_beam_theta, * d_beam_phi;

    // 1) harp data.
    cudaMalloc((void**)&d_ant_x, num_ant * sizeof(float));
    cudaMalloc((void**)&d_ant_y, num_ant * sizeof(float));
    cudaMalloc((void**)&d_ant_z, num_ant * sizeof(float));
    cudaMalloc((void**)&d_coeffs, num_ant * num_ant * num_mbf * sizeof(float2));
    cudaMalloc((void**)&d_alpha_te, num_mbf * max_order * (2 * max_order + 1) * sizeof(float2));
    cudaMalloc((void**)&d_alpha_tm, num_mbf * max_order * (2 * max_order + 1) * sizeof(float2));
    cudaMemcpy(d_ant_x, ant_x, num_ant * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ant_y, ant_y, num_ant * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ant_z, ant_z, num_ant * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_coeffs, coeffs, num_ant * num_ant * num_mbf * sizeof(float2), cudaMemcpyHostToDevice);
    cudaMemcpy(d_alpha_te, alpha_te, num_mbf * max_order * (2 * max_order + 1) * sizeof(float2), cudaMemcpyHostToDevice);
    cudaMemcpy(d_alpha_tm, alpha_tm, num_mbf * max_order * (2 * max_order + 1) * sizeof(float2), cudaMemcpyHostToDevice);

    // 2) Beam data.
    cudaMalloc((void**)&d_theta, num_dir * sizeof(float));
    cudaMalloc((void**)&d_phi, num_dir * sizeof(float));
    cudaMalloc((void**)&d_weights, num_ant * sizeof(float2));
    cudaMemcpy(d_theta, theta, num_dir * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_phi, phi, num_dir * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_weights, weights, num_ant * sizeof(float2), cudaMemcpyHostToDevice);

    // 3) Allocate output beam values.
    cudaMalloc((void**)&d_beam_theta, num_dir * sizeof(float2));
    cudaMalloc((void**)&d_beam_phi, num_dir * sizeof(float2));

    /*
     * Allocate memory for intermediate output variables from step 1 and step 2
     */
    float2* d_beam_coeffs;
    float2* d_pth, * d_pph;
    cudaMalloc((void**)&d_beam_coeffs, num_mbf * num_ant * sizeof(float2));
    cudaMalloc((void**)&d_pth, num_mbf * num_dir * sizeof(float2));
    cudaMalloc((void**)&d_pph, num_mbf * num_dir * sizeof(float2));

    /*
     * STEP 1: Evaluate the MBF coefficients of each beam.
     */
    cudaEventRecord(start);
    harp_evaluate_beam_coeffs_cuda_float(num_mbf, num_ant, d_weights, d_coeffs, d_beam_coeffs);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&msecs, start, stop);
    float time_step1 = msecs * 1e-3; // from msecs to secs

    /*
     * STEP 2: Evaluate the MBF patterns.
     */
    cudaEventRecord(start);
    float* d_poly;
    float2* d_ee, * d_qq, * d_dd;
    cudaMalloc((void**)&d_poly, num_dir * max_order * (max_order + 1) * sizeof(float));
    cudaMalloc((void**)&d_ee, num_dir * (2 * max_order + 1) * sizeof(float2));
    cudaMalloc((void**)&d_qq, num_dir * max_order * (2 * max_order + 1) * sizeof(float2));
    cudaMalloc((void**)&d_dd, num_dir * max_order * (2 * max_order + 1) * sizeof(float2));
    harp_precompute_smodes_mbf_cuda_float(max_order, num_dir, d_theta, d_phi, d_poly, d_ee, d_qq, d_dd);
    harp_reconstruct_smodes_mbf_cuda_float(max_order, num_dir, num_mbf, d_alpha_te, d_alpha_tm, d_qq, d_dd, d_pth, d_pph);
    cudaFree(d_poly);
    cudaFree(d_ee);
    cudaFree(d_qq);
    cudaFree(d_dd);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&msecs, start, stop);
    float time_step2 = msecs * 1e-3;

    /*
     * STEP 3: Assemble each beam from MBF patterns and MBF coefficients.
     */
    cudaEventRecord(start);
    float2* d_phase_fac;
    cudaMalloc((void**)&d_phase_fac, num_dir * num_ant * sizeof(float2));
    harp_evaluate_phase_fac_cuda_float(num_dir, num_ant, freq, d_theta, d_phi, d_ant_x, d_ant_y, d_ant_z, d_phase_fac);
    harp_assemble_station_beam_cuda_float(num_mbf, num_ant, num_dir, d_beam_coeffs, d_pth, d_pph, d_phase_fac, 1, 0, 0, d_beam_theta, d_beam_phi);
    cudaFree(d_phase_fac);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&msecs, start, stop);
    float time_step3 = msecs * 1e-3;

    /*
     * Free outputs from step 1 and 2.
     *
     * Note: in case multiple station beams (different antenna positions
     * or weights) need to be evaluated at the same directions,
     * pth and pph can be kept in memory.
     */
    cudaFree(d_pth);
    cudaFree(d_pph);
    cudaFree(d_beam_coeffs);

    /*
     * Copy beam values to host and free device memory.
     */
    cudaMemcpy(beam_theta, d_beam_theta, num_dir * sizeof(float2), cudaMemcpyDeviceToHost);
    cudaMemcpy(beam_phi, d_beam_phi, num_dir * sizeof(float2), cudaMemcpyDeviceToHost);

    cudaFree(d_beam_theta);
    cudaFree(d_beam_phi);
    cudaFree(d_phi);
    cudaFree(d_theta);
    cudaFree(d_ant_x);
    cudaFree(d_ant_y);
    cudaFree(d_ant_z);
    cudaFree(d_alpha_tm);
    cudaFree(d_alpha_te);
    cudaFree(d_coeffs);
    cudaFree(d_weights);

    printf("eval beam coeffs : %f secs \n", time_step1);
    printf("eval mbf patterns : %f secs \n", time_step2);
    printf("assemble station beam : %f secs \n", time_step3);
}

void cast_array_double_to_float(float* array2, double* array1, int len)
{
    for (int n = 0; n < len; n++)
    {
        array2[n] = (float) array1[n];
    }
}

void cast_array_double2_to_float2(float2* array2, double2* array1, int len)
{
    for (int n = 0; n < len; n++)
    {
        array2[n].x = (float) array1[n].x;
        array2[n].y = (float) array1[n].y;
    }
}

void cast_array_float2_to_double2(double2* array2, float2* array1, int len)
{
    for (int n = 0; n < len; n++)
    {
        array2[n].x = (double) array1[n].x;
        array2[n].y = (double) array1[n].y;
    }
}

int main(int argc, const char* argv[])
{
    /*
     * Assign user parameters.
     */

    const char* run_mode, * level, *filename;

    if (argc > 1)
    {
        run_mode = argv[1]; // run_mode can be "GPU" or "CPU"
        level = argv[2]; // level can be element or station
        filename = argv[3]; // output file with beam values
    }
    else
    {
        printf("not enough arguments \n");
        return 0;
    }

    char filename_harp[80], filename_weights[80], filename_output[80], filename_directions[80];
    strcpy(filename_harp,"harp_");
    strcat(filename_harp,filename);
    strcpy(filename_output, "beams_");
    strcat(filename_output, filename);
    strcpy(filename_directions, "directions_");
    strcat(filename_directions, filename);
    strcpy(filename_weights, "weights_");
    strcat(filename_weights, filename);

    /*
     * Load harp and beam data.
     */

    double freq; // frequency in Hz
    int max_order; // max order of spherical-wave decomposition of the MBF patterns
    int num_mbf; // number of mbfs
    int num_ant; // number of antenna
    read_harp_attributes_double(filename_harp, &freq, &num_ant, &num_mbf, &max_order);

    double* ant_x = (double*)malloc(num_ant * sizeof(double));// x-y antenna positions in local coordinates
    double* ant_y = (double*)malloc(num_ant * sizeof(double));
    double* ant_z = (double*)malloc(num_ant * sizeof(double));
    double2* coeffs_pola = (double2*) malloc(num_mbf * num_ant * num_ant * sizeof(double2)); // MBF coefficients of port "pola" of each antenna
    double2* coeffs_polb = (double2*) malloc(num_mbf * num_ant * num_ant * sizeof(double2)); // MBF coefficients of port "polb" of each antenna
    double2* alpha_te = (double2*) malloc(num_mbf * max_order * (2 * max_order + 1) * sizeof(double2)); // te spherical-wave coefficients of the MBF patterns
    double2* alpha_tm = (double2*) malloc(num_mbf * max_order * (2 * max_order + 1) * sizeof(double2)); // tm spherical-wave coefficients of the MBF patterns
    clock_t begin = clock();
    read_harp_datasets_double(filename_harp, ant_x,ant_y,ant_z, alpha_te, alpha_tm, coeffs_pola, coeffs_polb);

    double time_load_harp_data = (double)(clock() - begin) / CLOCKS_PER_SEC;

    printf("Loading harp data took: %.3f s\n", time_load_harp_data);

    /*
     * Load directions.
     */

    int num_dir; // number of directions at which the beam needs to be evaluated
    read_directions_attributes_double(filename_directions, &num_dir);
    double* theta = (double*)malloc(num_dir * sizeof(double)); // sky directions
    double* phi = (double*)malloc(num_dir * sizeof(double));
    read_directions_datasets_double(filename_directions, theta, phi);

    /*
     * Beam evaluation starts here.
     */
    int num_beams = 1;
    if (strcmp(level, "element")==0)
    {
        num_beams = num_ant;
    }

    // casting to inputs to float

    float freq_float = (float) freq;
    float* theta_float = (float*)malloc(num_dir * sizeof(float)); // sky directions
    float* phi_float = (float*)malloc(num_dir * sizeof(float));
    float* ant_x_float = (float*)malloc(num_ant * sizeof(float));
    float* ant_y_float = (float*)malloc(num_ant * sizeof(float));
    float* ant_z_float = (float*)malloc(num_ant * sizeof(float));
    float2* coeffs_pola_float = (float2*) malloc(num_mbf * num_ant * num_ant  * sizeof(float2));
    float2* coeffs_polb_float = (float2*) malloc(num_mbf * num_ant * num_ant * sizeof(float2));
    float2* alpha_te_float = (float2*) malloc(num_mbf * max_order * (2 * max_order + 1) * sizeof(float2));
    float2* alpha_tm_float = (float2*) malloc(num_mbf * max_order * (2 * max_order + 1) * sizeof(float2));

    cast_array_double_to_float(phi_float,phi,num_dir);
    cast_array_double_to_float(theta_float,theta,num_dir);
    cast_array_double_to_float(ant_x_float,ant_x,num_ant);
    cast_array_double_to_float(ant_y_float,ant_y,num_ant);
    cast_array_double_to_float(ant_z_float,ant_z,num_ant);
    cast_array_double2_to_float2(coeffs_pola_float,coeffs_pola,num_mbf * num_ant * num_ant );
    cast_array_double2_to_float2(coeffs_polb_float,coeffs_polb,num_mbf * num_ant * num_ant );
    cast_array_double2_to_float2(alpha_te_float,alpha_te,num_mbf * max_order * (2 * max_order + 1) );
    cast_array_double2_to_float2(alpha_tm_float,alpha_tm,num_mbf * max_order * (2 * max_order + 1) );

    float2* beam_theta_pola_float = (float2*) malloc(num_beams * num_dir * sizeof(float2));
    float2* beam_phi_pola_float = (float2*) malloc(num_beams *num_dir * sizeof(float2));
    float2* beam_theta_polb_float = (float2*) malloc(num_beams * num_dir * sizeof(float2));
    float2* beam_phi_polb_float = (float2*) malloc(num_beams * num_dir * sizeof(float2));

    if (strcmp(level, "station")==0) // evaluate a station beam
    {
        double2* weights_pola = (double2*) malloc(num_ant * sizeof(double2)); // beamforming weights of ports called "pola" (dual-polarized antenna)
        double2* weights_polb = (double2*) malloc(num_ant * sizeof(double2)); // beamforming weights of ports called "polb"
        read_weights_datasets_double(filename_weights, weights_pola, weights_polb);

        float2* weights_pola_float = (float2*) malloc(num_ant * sizeof(float2)); // beamforming weights of ports called "pola" (dual-polarized antenna)
        float2* weights_polb_float = (float2*) malloc(num_ant * sizeof(float2)); // beamforming weights of ports called "polb"
        cast_array_double2_to_float2(weights_pola_float,weights_pola,num_ant);
        cast_array_double2_to_float2(weights_polb_float,weights_polb,num_ant);

        if (strcmp(run_mode, "CPU") == 0) // running the single-threaded version
        {
            evaluate_station_beam_cpu(freq_float, max_order, num_dir, num_ant, num_mbf, theta_float, phi_float, ant_x_float, ant_y_float, ant_z_float, alpha_te_float, alpha_tm_float, coeffs_pola_float, weights_pola_float, beam_theta_pola_float, beam_phi_pola_float);
            evaluate_station_beam_cpu(freq_float, max_order, num_dir, num_ant, num_mbf, theta_float, phi_float, ant_x_float, ant_y_float, ant_z_float, alpha_te_float, alpha_tm_float, coeffs_polb_float, weights_polb_float, beam_theta_polb_float, beam_phi_polb_float);
        }
        else if (strcmp(run_mode, "GPU") == 0) // running the GPU version
        {
            evaluate_station_beam_gpu(freq_float, max_order, num_dir, num_ant, num_mbf, theta_float, phi_float, ant_x_float, ant_y_float, ant_z_float, alpha_te_float, alpha_tm_float, coeffs_pola_float, weights_pola_float, beam_theta_pola_float, beam_phi_pola_float);
            evaluate_station_beam_gpu(freq_float, max_order, num_dir, num_ant, num_mbf, theta_float, phi_float, ant_x_float, ant_y_float, ant_z_float, alpha_te_float, alpha_tm_float, coeffs_polb_float, weights_polb_float, beam_theta_polb_float, beam_phi_polb_float);
        }
        free(weights_pola_float);
        free(weights_polb_float);
        free(weights_pola);
        free(weights_polb);
    }
    else if (strcmp(level, "element")==0)
    {
        if (strcmp(run_mode, "CPU") == 0) // running the single-threaded version
        {
            evaluate_eeps_cpu(freq_float, max_order, num_dir, num_ant, num_mbf, theta_float, phi_float, ant_x_float, ant_y_float, ant_z_float, alpha_te_float, alpha_tm_float, coeffs_pola_float, beam_theta_pola_float, beam_phi_pola_float);
            evaluate_eeps_cpu(freq_float, max_order, num_dir, num_ant, num_mbf, theta_float, phi_float, ant_x_float, ant_y_float, ant_z_float, alpha_te_float, alpha_tm_float, coeffs_polb_float, beam_theta_polb_float, beam_phi_polb_float);
        }
        else if (strcmp(run_mode, "GPU") == 0) // running the GPU version
        {
            // permute harp's input coefficients to benefit from a memory efficient matrix-matrix product on gpus
            float2* coeffs_pola_perm = (float2*) malloc(num_mbf * num_ant * num_ant * sizeof(float2));
            float2* coeffs_polb_perm = (float2*) malloc(num_mbf * num_ant * num_ant * sizeof(float2));
            permute_harp_coeffs(num_ant, num_mbf, coeffs_pola_float, coeffs_pola_perm);
            permute_harp_coeffs(num_ant, num_mbf, coeffs_polb_float, coeffs_polb_perm);

            evaluate_eeps_gpu(freq_float, max_order, num_dir, num_ant, num_mbf, theta_float, phi_float, ant_x_float, ant_y_float, ant_z_float, alpha_te_float, alpha_tm_float, coeffs_pola_float, beam_theta_pola_float, beam_phi_pola_float);
            evaluate_eeps_gpu(freq_float, max_order, num_dir, num_ant, num_mbf, theta_float, phi_float, ant_x_float, ant_y_float, ant_z_float, alpha_te_float, alpha_tm_float, coeffs_polb_float, beam_theta_polb_float, beam_phi_polb_float);

            free(coeffs_pola_perm);
            free(coeffs_polb_perm);        
        }
    }
    free(phi_float);
    free(theta_float);
    free(ant_x_float);
    free(ant_y_float);
    free(ant_z_float);
    free(alpha_tm_float);
    free(alpha_te_float);
    free(coeffs_pola_float);
    free(coeffs_polb_float);

    double2* beam_theta_pola = (double2*) malloc(num_beams * num_dir * sizeof(double2));
    double2* beam_phi_pola = (double2*) malloc(num_beams *num_dir * sizeof(double2));
    double2* beam_theta_polb = (double2*) malloc(num_beams * num_dir * sizeof(double2));
    double2* beam_phi_polb = (double2*) malloc(num_beams * num_dir * sizeof(double2));
    cast_array_float2_to_double2(beam_phi_pola,beam_phi_pola_float,num_beams * num_dir);
    cast_array_float2_to_double2(beam_theta_pola,beam_theta_pola_float,num_beams * num_dir);
    cast_array_float2_to_double2(beam_phi_polb,beam_phi_polb_float,num_beams * num_dir);
    cast_array_float2_to_double2(beam_theta_polb,beam_theta_polb_float,num_beams * num_dir);
    free(beam_theta_pola_float);
    free(beam_phi_pola_float);
    free(beam_theta_polb_float);
    free(beam_phi_polb_float);

    write_beam_data_double(filename_output, num_dir, num_beams, theta, phi,
        beam_theta_pola, beam_phi_pola, beam_theta_polb, beam_phi_polb);

    free(beam_theta_pola);
    free(beam_phi_pola);
    free(beam_theta_polb);
    free(beam_phi_polb);

    free(phi);
    free(theta);
    free(ant_x);
    free(ant_y);
    free(ant_z);
    free(alpha_tm);
    free(alpha_te);
    free(coeffs_pola);
    free(coeffs_polb);

    return 0;
}
