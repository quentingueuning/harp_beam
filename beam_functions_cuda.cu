/* See the LICENSE file at the top-level directory of this distribution. */

#include <cmath>
#include <stdint.h>
#include "vector_types.h"
#include "harp_beam.h"

#define C0 299792458
#define PI 3.141592653589793

/*
 * STEP 1
 */

template <typename inType2>
__global__ void harp_evaluate_beam_coeffs_kernel(
    const int num_mbf,
    const int num_ant,
    const inType2* const __restrict__ weight,
    const inType2* const __restrict__ coeffs,
    inType2* __restrict__ beam_coeffs)
{
    const int i_out = blockDim.x * blockIdx.x + threadIdx.x;
    if (i_out >= num_ant * num_mbf) return;

    // Initialization of beam_coeffs.
    inType2 beam_coeff_local;
    beam_coeff_local.x = beam_coeff_local.y = 0.0;

    // Computation of an entry "i_out" of beam_coeff from vector-vector
    // product coeffs(iout,:) * weights.
    for (int ia = 0; ia < num_ant; ++ia)
    {
        const inType2 w = weight[ia];
        const inType2 coeff = coeffs[ia * num_ant * num_mbf + i_out];
        beam_coeff_local.x += coeff.x * w.x - coeff.y * w.y;
        beam_coeff_local.y += coeff.y * w.x + coeff.x * w.y;
    }
    beam_coeffs[i_out] = beam_coeff_local;
}

void harp_evaluate_beam_coeffs_cuda_float(
    const int num_mbf,
    const int num_ant,
    const float2* const __restrict__ weights,
    const float2* const __restrict__ coeffs,
    float2* __restrict__ beam_coeffs)
{
    const int num_threads = 256;
    dim3 blockSize(num_threads, 1, 1);
    dim3 gridSize((num_ant * num_mbf + num_threads - 1) / num_threads, 1, 1);
    harp_evaluate_beam_coeffs_kernel << < gridSize, blockSize >> > (
        num_mbf, num_ant, weights, coeffs, beam_coeffs);
}

void harp_evaluate_beam_coeffs_cuda_double(
    const int num_mbf,
    const int num_ant,
    const double2* const __restrict__ weights,
    const double2* const __restrict__ coeffs,
    double2* __restrict__ beam_coeffs)
{
    const int num_threads = 256;
    dim3 blockSize(num_threads, 1, 1);
    dim3 gridSize((num_ant * num_mbf + num_threads - 1) / num_threads, 1, 1);
    harp_evaluate_beam_coeffs_kernel << < gridSize, blockSize >> > (
        num_mbf, num_ant, weights, coeffs, beam_coeffs);
}

/*
 * STEP 2
 */

template <typename inType>
__global__ void harp_legendre_multi_kernel(
    const int max_order,
    const int num_dir,
    const inType* const __restrict__ theta,
    inType* __restrict__ poly)
{
    const int i_dir = blockDim.x * blockIdx.x + threadIdx.x;
    if (i_dir >= num_dir) return;

    const inType th = theta[i_dir];
    inType sin_theta = sin(th);
    inType cos_theta = cos(th);

    for (int n = 1; n <= max_order; ++n)
    {
        // Constant needed in the computation of pnn
        inType c = 1.0;
        for (int i = 1; i <= n; i++)
        {
            c *= (1.0 - 1.0 / ((inType)i * 2));
        }

        const int i_leg = (max_order + 1) * (num_dir * (n - 1) + i_dir);

        if (sin_theta != 0.0)
        {
            const inType cotan_theta = cos_theta / sin_theta;

            // Computation of two last polynomials pnn and pnn-1
            poly[i_leg + n] = sqrt(c) * pow(-sin_theta, (inType)n); // pnn
            poly[i_leg + n - 1] = -2.0 * poly[i_leg + n] * cotan_theta * n / sqrt(2.0 * n); // pnn-1

            // Three-term downwards recurrence relation.
            for (int m = n - 2; m > -1; m--)
            {
                poly[i_leg + m] = (-2.0 * poly[i_leg + m + 1] * cotan_theta * (inType)(m + 1)
                    - poly[i_leg + m + 2] * sqrt((inType)(n + m + 2) * (n - m - 1))) / sqrt((inType)(n + m + 1) * (n - m));
            }
        }
        else
        {
            // pn0(1) = cos_theta.^n
            poly[i_leg + 0] = pow(cos_theta, (inType)n);

            // All other polynomials are 0.
            for (int m = 1; m <= n; m++)
            {
                poly[i_leg + m] = 0.0;
            }
        }
    }
}

template <typename inType, typename inType2>
__global__ void harp_evaluate_exp_phi_m_kernel(
    const int max_order,
    const int num_dir,
    const inType* const __restrict__ phi,
    inType2* __restrict__ ee)
{
    const int i_dir = blockDim.x * blockIdx.x + threadIdx.x;
    if (i_dir >= num_dir) return;

    for (int m = -max_order; m <= max_order; m++)
    {
        const inType phi_m = m * phi[i_dir];
        inType sin_phi_m = sin(phi_m);
        inType cos_phi_m = cos(phi_m);

        const int index = (m + max_order) * num_dir + i_dir;
        ee[index].x = cos_phi_m;
        ee[index].y = sin_phi_m;
    }
}

template <typename inType, typename inType2>
__global__ void harp_evaluate_smodes_dd_qq_kernel(
    const int max_order,
    const int num_dir,
    const inType* const __restrict__ poly,
    const inType2* const __restrict__ ee,
    inType2* __restrict__ qq,
    inType2* __restrict__ dd)
{
    const int i_dir = blockDim.x * blockIdx.x + threadIdx.x;
    if (i_dir >= num_dir) return;

    for (int n = 1; n <= max_order; n++)
    {
        // Normalization factor.
        const inType Nf = 1.0 / sqrt(4.0 * PI * n * (n + 1.0) / (2.0 * n + 1.0));

        for (int m = -n; m <= n; m++)
        {
            // Loading pre-computed exp(j*m*phi)
            const inType2 eem = ee[(m + max_order) * num_dir + i_dir];

            // Computation of m * Pnm/sin_theta from precomputed
            // Legendre polynomials in array "poly"
            inType m_pnmd;
            const int i_n1m = (max_order + 1) * (num_dir * (n - 2) + i_dir) + abs(m); // index of p(n-1,m)
            if (m == 0)
            {
                m_pnmd = 0;
            }
            else if ((abs(m) + 1) > (n - 1)) // when pn-1m+1 = 0
            {
                if (n == 1) // when p(0,1) = 1
                {
                    m_pnmd =  -1.0 / sqrt(2.0);
                }
                else
                {
                    m_pnmd = (-1.0 / 2.0) * sqrt((inType) (n + abs(m) - 1) * (n + abs(m))) * poly[i_n1m - 1];
                }
            }
            else
            {
                m_pnmd = (-1.0 / 2.0) * (sqrt((inType)(n - abs(m) - 1) * (n - abs(m))) * poly[i_n1m + 1] + sqrt((inType)(n + abs(m) - 1) * (n + abs(m))) * poly[i_n1m - 1]);
            }

            // Just flip sign for pnm with negative degree m.
            if (m < 0) m_pnmd *= -1.0;

            // Computation of dpnm/d(cos_theta) * sin_theta
            inType dpnm;
            const int i_nm = (max_order + 1) * (num_dir * (n - 1) + i_dir) + abs(m); // index of p(n,m)
            if (abs(m) == 0) // when p(n,m-1) = p(n,-1)
            {
                dpnm = -sqrt((inType) (n + abs(m) + 1) * (n - abs(m))) * poly[i_nm + 1];
            }
            else if (abs(m) == n) // when p(n,m+1) = 0
            {
                dpnm = 1.0 / 2.0 * sqrt((inType)(n + abs(m)) * (n - abs(m) + 1)) * poly[i_nm - 1];
            }
            else
            {
                dpnm = 1.0 / 2.0 * (sqrt((inType)(n + abs(m)) * (n - abs(m) + 1)) * poly[i_nm - 1] - sqrt((inType)(n + abs(m) + 1) * (n - abs(m))) * poly[i_nm + 1]);
            }

            // Compute and store value of qq and dd for spherical mode of index "i_smodes
            const int i_smode = num_dir * ((2 * max_order + 1) * (n - 1) + (m + max_order)) + i_dir;
            qq[i_smode].x = -eem.x * dpnm * Nf;
            qq[i_smode].y = -eem.y * dpnm * Nf;
            dd[i_smode].x = eem.x * m_pnmd * Nf;
            dd[i_smode].y = eem.y * m_pnmd * Nf;
        }
    }
}

template <typename inType2>
__global__ void harp_reconstruct_smodes_kernel(
    int max_order,
    int num_dir,
    int num_mbf,
    const inType2* const __restrict__ alpha_te,
    const inType2* const __restrict__ alpha_tm,
    const inType2* const __restrict__ qq,
    const inType2* const __restrict__ dd,
    inType2* __restrict__ p_theta,
    inType2* __restrict__ p_phi)
{
    const int i_mbf = blockDim.y * blockIdx.y + threadIdx.y;
    if (i_mbf >= num_mbf) return;
    const int i_dir = blockDim.x * blockIdx.x + threadIdx.x;
    if (i_dir >= num_dir) return;

    // Initialization of MBF patterns p_theta and p_phi.
    inType2 p_theta_local, p_phi_local;
    p_theta_local.x = p_theta_local.y = 0.0;
    p_phi_local.x = p_phi_local.y = 0.0;

    for (int n = 1; n <= max_order; n++)
    {
        for (int m = -n; m <= n; m++)
        {
            // Load precomputed values of qq and dd for smode (n,m)
            const int i_qq = num_dir * ((2 * max_order + 1) * (n - 1) + (m + max_order)) + i_dir;
            const inType2 qq_ = qq[i_qq];
            const inType2 dd_ = dd[i_qq];

            // Load spherical-wave te-,tm- coefficients alpha_te, alpha_tm
            // associated with the patterns of mbf "i_mbf"
            const int i_alpha = ((n - 1) * (2 * max_order + 1) + m + n) * num_mbf + i_mbf;
            const inType2 a_te = alpha_te[i_alpha];
            const inType2 a_tm = alpha_tm[i_alpha];

            // Evaluate and store the theta and phi polarizations of
            // the MBF patterns using coefficients and dd,qq values.
            p_theta_local.x += a_tm.x * qq_.x - a_tm.y * qq_.y + a_te.x * dd_.y + a_te.y * dd_.x;
            p_theta_local.y += a_tm.x * qq_.y + a_tm.y * qq_.x - a_te.x * dd_.x + a_te.y * dd_.y;
            p_phi_local.x += a_te.x * qq_.x - a_te.y * qq_.y - a_tm.x * dd_.y - a_tm.y * dd_.x;
            p_phi_local.y += a_te.x * qq_.y + a_te.y * qq_.x + a_tm.x * dd_.x - a_tm.y * dd_.y;
        }
    }

    const int i_out = i_mbf * num_dir + i_dir;
    p_theta[i_out] = p_theta_local;
    p_phi[i_out] = p_phi_local;
}

void harp_precompute_smodes_mbf_cuda_float(
    const int max_order,
    const int num_dir,
    const float* const __restrict__ theta,
    const float* const __restrict__ phi,
    float* __restrict__ poly,
    float2* __restrict__ ee,
    float2* __restrict__ qq,
    float2* __restrict__ dd)
{
    const int num_threads = 256;
    dim3 blockSize(num_threads, 1, 1);
    dim3 gridSize((num_dir + num_threads - 1) / num_threads, 1, 1);
    harp_legendre_multi_kernel << < gridSize, blockSize >> > (
        max_order, num_dir, theta, poly);
    harp_evaluate_exp_phi_m_kernel << < gridSize, blockSize >> > (
        max_order, num_dir, phi, ee);
    harp_evaluate_smodes_dd_qq_kernel << < gridSize, blockSize >> > (
        max_order, num_dir, poly, ee, qq, dd);
}

void harp_precompute_smodes_mbf_cuda_double(
    const int max_order,
    const int num_dir,
    const double* const __restrict__ theta,
    const double* const __restrict__ phi,
    double* __restrict__ poly,
    double2* __restrict__ ee,
    double2* __restrict__ qq,
    double2* __restrict__ dd)
{
    const int num_threads = 256;
    dim3 blockSize(num_threads, 1, 1);
    dim3 gridSize((num_dir + num_threads - 1) / num_threads, 1, 1);

    harp_legendre_multi_kernel << < gridSize, blockSize >> > (
        max_order, num_dir, theta, poly);
    harp_evaluate_exp_phi_m_kernel << < gridSize, blockSize >> > (
        max_order, num_dir, phi, ee);
    harp_evaluate_smodes_dd_qq_kernel << < gridSize, blockSize >> > (
        max_order, num_dir, poly, ee, qq, dd);
}

void harp_reconstruct_smodes_mbf_cuda_float(
    const int max_order,
    const int num_dir,
    const int num_mbf,
    const float2* const __restrict__ alpha_te,
    const float2* const __restrict__ alpha_tm,
    const float2* const __restrict__ qq,
    const float2* const __restrict__ dd,
    float2* __restrict__ p_theta,
    float2* __restrict__ p_phi)
{
    const int num_threads = 256;
    dim3 blockSize(num_threads, 1, 1);
    dim3 gridSize((num_dir + num_threads - 1) / num_threads, num_mbf, 1);
    harp_reconstruct_smodes_kernel << < gridSize, blockSize >> > (max_order, num_dir, num_mbf, alpha_te, alpha_tm,
        qq, dd, p_theta, p_phi);
}

void harp_reconstruct_smodes_mbf_cuda_double(
    const int max_order,
    const int num_dir,
    const int num_mbf,
    const double2* const __restrict__ alpha_te,
    const double2* const __restrict__ alpha_tm,
    const double2* const __restrict__ qq,
    const double2* const __restrict__ dd,
    double2* __restrict__ p_theta,
    double2* __restrict__ p_phi)
{
    const int num_threads = 256;
    dim3 blockSize(num_threads, 1, 1);
    dim3 gridSize((num_dir + num_threads - 1) / num_threads, num_mbf, 1);
    harp_reconstruct_smodes_kernel << < gridSize, blockSize >> > (max_order, num_dir, num_mbf, alpha_te, alpha_tm,
        qq, dd, p_theta, p_phi);
}

/*
 * STEP 3
 */

template <typename inType, typename inType2>
__global__ void harp_evaluate_phase_fac_kernel(
    const int num_dir,
    const int num_ant,
    const inType freq,
    const inType* const __restrict__ theta,
    const inType* const __restrict__ phi,
    const inType* const __restrict__ ant_x,
    const inType* const __restrict__ ant_y,
    const inType* const __restrict__ ant_z,
    inType2* __restrict__ phase_fac)
{
    const int i_dir = blockDim.y * blockIdx.y + threadIdx.y;
    const int i_ant = blockDim.x * blockIdx.x + threadIdx.x;
    if (i_dir >= num_dir || i_ant >= num_ant) return;

    // wavenumber
    const inType k0 = 2.0 * PI * freq / C0;

    // cartesian spectral coordinates
    const inType kx = k0 * sin(theta[i_dir]) * cos(phi[i_dir]);
    const inType ky = k0 * sin(theta[i_dir]) * sin(phi[i_dir]);
    const inType kz = k0 * cos(theta[i_dir]);

    // exp(j (kx * xant + ky * yant + kz * zant))
    const int index = i_dir * num_ant + i_ant;
    const inType phase =
        kx * ant_x[i_ant] + ky * ant_y[i_ant] + kz * ant_z[i_ant];
    phase_fac[index].x = cos(phase);
    phase_fac[index].y = sin(phase);
}

void harp_evaluate_phase_fac_cuda_float(
    const int num_dir,
    const int num_ant,
    const float freq,
    const float* const __restrict__ theta,
    const float* const __restrict__ phi,
    const float* const __restrict__ ant_x,
    const float* const __restrict__ ant_y,
    const float* const __restrict__ ant_z,
    float2* __restrict__ phase_fac)
{

    dim3 blockSize(SUBMATRIX_SIZE, SUBMATRIX_SIZE, 1);
    dim3 gridSize(
        (num_ant + SUBMATRIX_SIZE - 1) / SUBMATRIX_SIZE,
        (num_dir + SUBMATRIX_SIZE - 1) / SUBMATRIX_SIZE,
        1
    );

    harp_evaluate_phase_fac_kernel << < gridSize, blockSize >> > (
        num_dir, num_ant, freq, theta, phi, ant_x, ant_y, ant_z, phase_fac);
}

void harp_evaluate_phase_fac_cuda_double(
    const int num_dir,
    const int num_ant,
    const double freq,
    const double* const __restrict__ theta,
    const double* const __restrict__ phi,
    const double* const __restrict__ ant_x,
    const double* const __restrict__ ant_y,
    const double* const __restrict__ ant_z,
    double2* __restrict__ phase_fac)
{
    dim3 blockSize(SUBMATRIX_SIZE, SUBMATRIX_SIZE, 1);
    dim3 gridSize(
        (num_ant + SUBMATRIX_SIZE - 1) / SUBMATRIX_SIZE,
        (num_dir + SUBMATRIX_SIZE - 1) / SUBMATRIX_SIZE,
        1
    );

    harp_evaluate_phase_fac_kernel << < gridSize, blockSize >> > (
        num_dir, num_ant, freq, theta, phi, ant_x, ant_y, ant_z, phase_fac);
}


template <typename inType2>
__global__ void harp_assemble_station_beam_kernel(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const inType2* const __restrict__ beam_coeffs,
    const inType2* const __restrict__ p_theta,
    const inType2* const __restrict__ p_phi,
    const inType2* const __restrict__ phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    inType2* beam_theta,
    inType2* beam_phi)
{
    const int i_dir = blockDim.x * blockIdx.x + threadIdx.x;
    if (i_dir >= num_dir) return;

    inType2 beam_theta_local = { 0.0, 0.0 };
    inType2 beam_phi_local = { 0.0, 0.0 };

    for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
    {
        // AF initialization
        inType2 AF_local = { 0.0, 0.0 };
        
        // Compute the array factors.
        for (int i_ant = 0; i_ant < num_ant; i_ant++)
        {
            const int64_t i_coe = i_ant * num_mbf + i_mbf;
            const int i_ph = i_dir * num_ant + i_ant;
            AF_local.x += phase_fac[i_ph].x * beam_coeffs[i_coe].x - phase_fac[i_ph].y * beam_coeffs[i_coe].y;
            AF_local.y += phase_fac[i_ph].x * beam_coeffs[i_coe].y + phase_fac[i_ph].y * beam_coeffs[i_coe].x;
        }

        // beam_th = sum_i_mbf p_theta(i_mbf) * AF(i_mbf)
        const int i_p = i_mbf * num_dir + i_dir;
        beam_theta_local.x += AF_local.x * p_theta[i_p].x - AF_local.y * p_theta[i_p].y;
        beam_theta_local.y += AF_local.x * p_theta[i_p].y + AF_local.y * p_theta[i_p].x;
        beam_phi_local.x += AF_local.x * p_phi[i_p].x - AF_local.y * p_phi[i_p].y;
        beam_phi_local.y += AF_local.x * p_phi[i_p].y + AF_local.y * p_phi[i_p].x;
    }
    beam_theta[stride * i_dir + beam_theta_offset] = beam_theta_local;
    beam_phi[stride * i_dir + beam_phi_offset] = beam_phi_local;
}

void harp_assemble_station_beam_cuda_float(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const float2* const __restrict__ beam_coeffs,
    const float2* const __restrict__ p_theta,
    const float2* const __restrict__ p_phi,
    const float2* const __restrict__ phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    float2* beam_theta,
    float2* beam_phi)
{
    const int num_threads = 256;
    dim3 blockSize(num_threads, 1, 1);
    dim3 gridSize((num_dir + num_threads - 1) / num_threads, 1, 1);
    harp_assemble_station_beam_kernel << < gridSize, blockSize >> > (
        num_mbf, num_ant, num_dir, beam_coeffs, p_theta, p_phi, phase_fac,
        stride, beam_theta_offset, beam_phi_offset, beam_theta, beam_phi);
}

void harp_assemble_station_beam_cuda_double(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const double2* const __restrict__ beam_coeffs,
    const double2* const __restrict__ p_theta,
    const double2* const __restrict__ p_phi,
    const double2* const __restrict__ phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    double2* beam_theta,
    double2* beam_phi)
{
    const int num_threads = 256;
    dim3 blockSize(num_threads, 1, 1);
    dim3 gridSize((num_dir + num_threads - 1) / num_threads, 1, 1);
    harp_assemble_station_beam_kernel << < gridSize, blockSize >> > (
        num_mbf, num_ant, num_dir, beam_coeffs, p_theta, p_phi, phase_fac,
        stride, beam_theta_offset, beam_phi_offset, beam_theta, beam_phi);
}


template<typename inType2>
__global__ void harp_assemble_element_beams_kernel(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const inType2* const __restrict__ beam_coeffs,
    const inType2* const __restrict__ p_theta,
    const inType2* const __restrict__ p_phi,
    const inType2* const __restrict__ phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    inType2* beam_theta,
    inType2* beam_phi
)
{
    __shared__ inType2 s_phase_fac[SUBMATRIX_SIZE][SUBMATRIX_SIZE];
    __shared__ inType2 s_beam_coeffs[SUBMATRIX_SIZE][SUBMATRIX_SIZE];

    inType2 beam_theta_local = {0, 0};
    inType2 beam_phi_local = {0, 0};
    const inType2 zero2 = {0, 0};

    int col, row;
    for (int m = 0; m < num_mbf; m++)
    {
        const int64_t shift = (int64_t)m * (int64_t)num_ant * (int64_t)num_ant;
        inType2 AF_local = {0, 0};
        int num_blocks = (num_ant + SUBMATRIX_SIZE - 1) / SUBMATRIX_SIZE;
        for (int i_block = 0; i_block < num_blocks; i_block++)
        {
            // Load tile of matrix A
            row = blockIdx.y * blockDim.y + threadIdx.y;
            col = i_block * blockDim.x + threadIdx.x;
            if (row < num_dir && col < num_ant)
            {
                s_phase_fac[threadIdx.y][threadIdx.x] = phase_fac[row * num_ant + col];
            }
            else
            {
                s_phase_fac[threadIdx.y][threadIdx.x] = zero2;
            }

            // Load tile of Matrix B
            row = i_block * blockDim.y + threadIdx.y;
            col = blockIdx.x * blockDim.x + threadIdx.x;
            if (row < num_ant && col < num_ant)
            {
                s_beam_coeffs[threadIdx.y][threadIdx.x] = beam_coeffs[shift + row * num_ant + col];
            }
            else
            {
                s_beam_coeffs[threadIdx.y][threadIdx.x] = zero2;
            }

            __syncthreads();

            // Matrix multiply
            for (int f = 0; f < SUBMATRIX_SIZE; f++)
            {
                inType2 A = s_phase_fac[threadIdx.y][f];
                inType2 B = s_beam_coeffs[f][threadIdx.x];
                AF_local.x += A.x * B.x - A.y * B.y;
                AF_local.y += A.x * B.y + A.y * B.x;
            }

            __syncthreads();
        }

        // multiply by pth, pph
        int i_p = m * num_dir + blockIdx.y * blockDim.y + threadIdx.y;
        inType2 p_th = p_theta[i_p];
        inType2 p_ph = p_phi[i_p];
        beam_theta_local.x += AF_local.x * p_th.x - AF_local.y * p_th.y;
        beam_theta_local.y += AF_local.x * p_th.y + AF_local.y * p_th.x;
        beam_phi_local.x += AF_local.x * p_ph.x - AF_local.y * p_ph.y;
        beam_phi_local.y += AF_local.x * p_ph.y + AF_local.y * p_ph.x;
    }

    // Store
    row = blockIdx.y * blockDim.y + threadIdx.y; // i_dir
    col = blockIdx.x * blockDim.x + threadIdx.x; // i_ant
    if (row < num_dir && col < num_ant)
    {
        // Multiply by conjugate of phase factor to remove phase gradient.
        const inType2 phase = phase_fac[row * num_ant + col];
        const inType2 b_theta_tmp = beam_theta_local;
        const inType2 b_phi_tmp = beam_phi_local;
        beam_theta_local.x = b_theta_tmp.x * phase.x + b_theta_tmp.y * phase.y;
        beam_theta_local.y = b_theta_tmp.y * phase.x - b_theta_tmp.x * phase.y;
        beam_phi_local.x = b_phi_tmp.x * phase.x + b_phi_tmp.y * phase.y;
        beam_phi_local.y = b_phi_tmp.y * phase.x - b_phi_tmp.x * phase.y;

        // Write to global memory.
        const int i_out = stride * (col * num_dir + row);
        beam_theta[i_out + beam_theta_offset] = beam_theta_local;
        beam_phi[i_out + beam_phi_offset] = beam_phi_local;
    }
}

void harp_assemble_element_beams_cuda_float(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const float2* const __restrict__ beam_coeffs,
    const float2* const __restrict__ p_theta,
    const float2* const __restrict__ p_phi,
    const float2* const __restrict__ phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    float2* beam_theta,
    float2* beam_phi)
{
    dim3 blockSize(SUBMATRIX_SIZE, SUBMATRIX_SIZE, 1);
    dim3 gridSize((num_ant + SUBMATRIX_SIZE - 1) / SUBMATRIX_SIZE,
        (num_dir + SUBMATRIX_SIZE - 1) / SUBMATRIX_SIZE, 1);

    harp_assemble_element_beams_kernel<<< gridSize, blockSize >>>(
        num_mbf, num_ant, num_dir,
        beam_coeffs, p_theta, p_phi, phase_fac,
        stride, beam_theta_offset, beam_phi_offset, beam_theta, beam_phi
    );
}

void harp_assemble_element_beams_cuda_double(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const double2* const __restrict__ beam_coeffs,
    const double2* const __restrict__ p_theta,
    const double2* const __restrict__ p_phi,
    const double2* const __restrict__ phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    double2* beam_theta,
    double2* beam_phi)
{
    dim3 blockSize(SUBMATRIX_SIZE, SUBMATRIX_SIZE, 1);
    dim3 gridSize((num_ant + SUBMATRIX_SIZE - 1) / SUBMATRIX_SIZE,
        (num_dir + SUBMATRIX_SIZE - 1) / SUBMATRIX_SIZE,1);

    harp_assemble_element_beams_kernel<<< gridSize, blockSize >>>(
        num_mbf, num_ant, num_dir,
        beam_coeffs, p_theta, p_phi, phase_fac,
        stride, beam_theta_offset, beam_phi_offset, beam_theta, beam_phi
    );
}
