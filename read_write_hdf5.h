/* See the LICENSE file at the top-level directory of this distribution. */

#ifdef __cplusplus
extern "C" {
#endif

void write_beam_data_double(
    const char* const __restrict__ filename,
    const int num_dir,
    const int num_beam,
    double* const __restrict__ theta,
    double* const __restrict__ phi,
    double2* const __restrict__ beam_theta_pola,
    double2* const __restrict__ beam_phi_pola,
    double2* const __restrict__ beam_theta_polb,
    double2* const __restrict__ beam_phi_polb);

void read_harp_attributes_double(
    const char* const __restrict__ filename,
    double* __restrict__ freq,
    int* __restrict__ num_ant,
    int* __restrict__ num_mbf,
    int* __restrict__ max_order);

void read_directions_attributes_double(
    const char* const __restrict__ filename,
    int* __restrict__ num_dir);

void read_directions_datasets_double(
    const char* const __restrict__ filename,
    double* __restrict__ theta,
    double* __restrict__ phi);

void read_weights_datasets_double(
    const char* const __restrict__ filename,
    double2* __restrict__ weights_pola,
    double2* __restrict__ weights_polb);

void read_harp_datasets_double(
    const char* const __restrict__ filename,
    double* __restrict__ ant_x,
    double* __restrict__ ant_y,
    double* __restrict__ ant_z,
    double2* __restrict__ alpha_te,
    double2* __restrict__ alpha_tm,
    double2* __restrict__ coeffs_pola,
    double2* __restrict__ coeffs_polb);

#ifdef __cplusplus
}
#endif
