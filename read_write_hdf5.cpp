/* See the LICENSE file at the top-level directory of this distribution. */

#include "hdf5.h"
#include "vector_types.h"
#include "read_write_hdf5.h"

void write_beam_data_double(
    const char* const __restrict__ filename,
    const int num_dir,
    const int num_beam,
    double* const __restrict__ theta,
    double* const __restrict__ phi,
    double2* const __restrict__ beam_theta_pola,
    double2* const __restrict__ beam_phi_pola,
    double2* const __restrict__ beam_theta_polb,
    double2* const __restrict__ beam_phi_polb)
{
    // Write attributes
    hid_t file_id, type_id;

    file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    // Write datasets
    hid_t dataset_id, dataspace_id, memspace;
    hsize_t dims1[2], dims2[2];

    dims1[0] = num_dir;
    dims1[1] = 1;
    dims2[0] = num_dir*num_beam;
    dims2[1] = 1;

    dataspace_id = H5Screate_simple(2, dims1, NULL);

    dataset_id = H5Dcreate2(file_id, "/theta", H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    memspace = H5Dget_space(dataset_id);
    H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, memspace, H5S_ALL, H5P_DEFAULT, theta);
    H5Dclose(dataset_id);

    dataset_id = H5Dcreate2(file_id, "/phi", H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    memspace = H5Dget_space(dataset_id);
    H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, memspace, H5S_ALL, H5P_DEFAULT, phi);
    H5Dclose(dataset_id);

    type_id = H5Tcopy(H5T_IEEE_F64LE);
    hid_t esize = H5Tget_size(type_id);
    hid_t etype = H5Tcreate(H5T_COMPOUND, 2 * esize);
    H5Tinsert(etype, "r", 0, type_id);
    H5Tinsert(etype, "i", esize, type_id);

    dataspace_id = H5Screate_simple(2, dims2, NULL);
    dataset_id = H5Dcreate2(file_id, "/beam_theta_pola", etype, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    memspace = H5Dget_space(dataset_id);
    H5Dwrite(dataset_id, etype, H5S_ALL, H5S_ALL, H5P_DEFAULT, beam_theta_pola);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);

    dataspace_id = H5Screate_simple(2, dims2, NULL);
    dataset_id = H5Dcreate2(file_id, "/beam_phi_pola", etype, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    memspace = H5Dget_space(dataset_id);
    H5Dwrite(dataset_id, etype, H5S_ALL, H5S_ALL, H5P_DEFAULT, beam_phi_pola);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);

    dataspace_id = H5Screate_simple(2, dims2, NULL);
    dataset_id = H5Dcreate2(file_id, "/beam_theta_polb", etype, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    memspace = H5Dget_space(dataset_id);
    H5Dwrite(dataset_id, etype, H5S_ALL, H5S_ALL, H5P_DEFAULT, beam_theta_polb);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);

    dataspace_id = H5Screate_simple(2, dims2, NULL);
    dataset_id = H5Dcreate2(file_id, "/beam_phi_polb", etype, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    memspace = H5Dget_space(dataset_id);
    H5Dwrite(dataset_id, etype, H5S_ALL, H5S_ALL, H5P_DEFAULT, beam_phi_polb);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);

    H5Tclose(etype);
    H5Fclose(file_id);
}

void read_harp_attributes_double(
    const char* const __restrict__ filename,
    double* __restrict__ freq,
    int* __restrict__ num_ant,
    int* __restrict__ num_mbf,
    int* __restrict__ max_order)
{
    hid_t  file_id, dataattr_id, datatype;

    file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT);

    dataattr_id = H5Aopen(file_id, "freq", H5P_DEFAULT);
    datatype = H5Aget_type(dataattr_id);
    H5Aread(dataattr_id, datatype, freq);
    H5Aclose(dataattr_id);

    dataattr_id = H5Aopen(file_id, "num_mbf", H5P_DEFAULT);
    datatype = H5Aget_type(dataattr_id);
    H5Aread(dataattr_id, datatype, num_mbf);
    H5Aclose(dataattr_id);

    dataattr_id = H5Aopen(file_id, "num_ant", H5P_DEFAULT);
    datatype = H5Aget_type(dataattr_id);
    H5Aread(dataattr_id, datatype, num_ant);
    H5Aclose(dataattr_id);

    dataattr_id = H5Aopen(file_id, "max_order", H5P_DEFAULT);
    datatype = H5Aget_type(dataattr_id);
    H5Aread(dataattr_id, datatype, max_order);
    H5Aclose(dataattr_id);

    H5Fclose(file_id);
}

void read_directions_attributes_double(
    const char* const __restrict__ filename,
    int* __restrict__ num_dir)
{
    hid_t file_id, dataattr_id, datatype;

    file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT);

    dataattr_id = H5Aopen(file_id, "num_dir", H5P_DEFAULT);
    datatype = H5Aget_type(dataattr_id);
    H5Aread(dataattr_id, datatype, num_dir);
    H5Aclose(dataattr_id);

    H5Fclose(file_id);
}

void read_directions_datasets_double(
    const char* const __restrict__ filename,
    double* __restrict__ theta,
    double* __restrict__ phi)
{
    hid_t file_id, dataset_id, datatype;

    file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT);

    dataset_id = H5Dopen(file_id, "theta", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, theta);
    H5Dclose(dataset_id);

    dataset_id = H5Dopen(file_id, "phi", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, phi);
    H5Dclose(dataset_id);

    H5Fclose(file_id);

}

void read_weights_datasets_double(
    const char* const __restrict__ filename,
    double2* __restrict__ weights_pola,
    double2* __restrict__ weights_polb)
{
    hid_t file_id, dataset_id, datatype;

    file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT);

    dataset_id = H5Dopen(file_id, "weights_pola", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, weights_pola);
    H5Dclose(dataset_id);

    dataset_id = H5Dopen(file_id, "weights_polb", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, weights_polb);
    H5Dclose(dataset_id);

    H5Fclose(file_id);

}

void read_harp_datasets_double(
    const char* const __restrict__ filename,
    double* __restrict__ ant_x,
    double* __restrict__ ant_y,
    double* __restrict__ ant_z,
    double2* __restrict__ alpha_te,
    double2* __restrict__ alpha_tm,
    double2* __restrict__ coeffs_pola,
    double2* __restrict__ coeffs_polb)
{
    hid_t  file_id, dataset_id, datatype;

    file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT);

    dataset_id = H5Dopen(file_id, "alpha_te", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, alpha_te);
    H5Dclose(dataset_id);

    dataset_id = H5Dopen(file_id, "alpha_tm", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, alpha_tm);
    H5Dclose(dataset_id);

    dataset_id = H5Dopen(file_id, "coeffs_pola", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, coeffs_pola);
    H5Dclose(dataset_id);

    dataset_id = H5Dopen(file_id, "coeffs_polb", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, coeffs_polb);
    H5Dclose(dataset_id);

    dataset_id = H5Dopen(file_id, "ant_x", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, ant_x);
    H5Dclose(dataset_id);

    dataset_id = H5Dopen(file_id, "ant_y", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, ant_y);
    H5Dclose(dataset_id);

    dataset_id = H5Dopen(file_id, "ant_z", H5P_DEFAULT);
    datatype = H5Dget_type(dataset_id);
    H5Dread(dataset_id, datatype, H5S_ALL, H5S_ALL, H5P_DEFAULT, ant_z);
    H5Dclose(dataset_id);

    H5Fclose(file_id);
}
