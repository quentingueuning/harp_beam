/* See the LICENSE file at the top-level directory of this distribution. */

#ifndef HARP_BEAM_H_
#define HARP_BEAM_H_

#ifndef RESTRICT
    #if defined(__cplusplus) && defined(__GNUC__)
        #define RESTRICT __restrict__
    #elif defined(_MSC_VER)
        #define RESTRICT __restrict
    #elif !defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L
        #define RESTRICT
    #else
        #define RESTRICT restrict
    #endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define SUBMATRIX_SIZE 16
/*
 * STEP 1
 */

void harp_evaluate_beam_coeffs_float(
    const int num_mbf,
    const int num_ant,
    const float2* const RESTRICT weights,
    const float2* const RESTRICT coeffs,
    float2* RESTRICT beam_coeffs);

void harp_evaluate_beam_coeffs_cuda_float(
    const int num_mbf,
    const int num_ant,
    const float2* const RESTRICT weights,
    const float2* const RESTRICT coeffs,
    float2* RESTRICT beam_coeffs);

void harp_evaluate_beam_coeffs_double(
    const int num_mbf,
    const int num_ant,
    const double2* const RESTRICT weights,
    const double2* const RESTRICT coeffs,
    double2* RESTRICT beam_coeffs);

void harp_evaluate_beam_coeffs_cuda_double(
    const int num_mbf,
    const int num_ant,
    const double2* const RESTRICT weights,
    const double2* const RESTRICT coeffs,
    double2* RESTRICT beam_coeffs);

/*
 * STEP 2
 */

void harp_precompute_smodes_mbf_float(
    const int max_order,
    const int num_dir,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    float* RESTRICT poly,
    float2* RESTRICT ee,
    float2* RESTRICT qq,
    float2* RESTRICT dd);

void harp_precompute_smodes_mbf_cuda_float(
    const int max_order,
    const int num_dir,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    float* RESTRICT poly,
    float2* RESTRICT ee,
    float2* RESTRICT qq,
    float2* RESTRICT dd);

void harp_reconstruct_smodes_mbf_float(
    const int max_order,
    const int num_dir,
    const int num_mbf,
    const float2* const RESTRICT alpha_te,
    const float2* const RESTRICT alpha_tm,
    const float2* const RESTRICT qq,
    const float2* const RESTRICT dd,
    float2* RESTRICT pth,
    float2* RESTRICT pph);

void harp_reconstruct_smodes_mbf_cuda_float(
    const int max_order,
    const int num_dir,
    const int num_mbf,
    const float2* const RESTRICT alpha_te,
    const float2* const RESTRICT alpha_tm,
    const float2* const RESTRICT qq,
    const float2* const RESTRICT dd,
    float2* RESTRICT pth,
    float2* RESTRICT pph);

void harp_precompute_smodes_mbf_double(
    const int max_order,
    const int num_dir,
    const double* const RESTRICT theta,
    const double* const RESTRICT phi,
    double* RESTRICT poly,
    double2* RESTRICT ee,
    double2* RESTRICT qq,
    double2* RESTRICT dd);

void harp_precompute_smodes_mbf_cuda_double(
    const int max_order,
    const int num_dir,
    const double* const RESTRICT theta,
    const double* const RESTRICT phi,
    double* RESTRICT poly,
    double2* RESTRICT ee,
    double2* RESTRICT qq,
    double2* RESTRICT dd);

void harp_reconstruct_smodes_mbf_double(
    const int max_order,
    const int num_dir,
    const int num_mbf,
    const double2* const RESTRICT alpha_te,
    const double2* const RESTRICT alpha_tm,
    const double2* const RESTRICT qq,
    const double2* const RESTRICT dd,
    double2* RESTRICT pth,
    double2* RESTRICT pph);

void harp_reconstruct_smodes_mbf_cuda_double(
    const int max_order,
    const int num_dir,
    const int num_mbf,
    const double2* const RESTRICT alpha_te,
    const double2* const RESTRICT alpha_tm,
    const double2* const RESTRICT qq,
    const double2* const RESTRICT dd,
    double2* RESTRICT pth,
    double2* RESTRICT pph);

/*
 * STEP 3
 */

void harp_evaluate_phase_fac_float(
    const int num_dir,
    const int num_ant,
    const float freq,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    const float* const RESTRICT ant_x,
    const float* const RESTRICT ant_y,
    const float* const RESTRICT ant_z,
    float2* RESTRICT phase_fac);

void harp_evaluate_phase_fac_cuda_float(
    const int num_dir,
    const int num_ant,
    const float freq,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    const float* const RESTRICT ant_x,
    const float* const RESTRICT ant_y,
    const float* const RESTRICT ant_z,
    float2* RESTRICT phase_fac);

void harp_assemble_station_beam_float(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const float2* const RESTRICT beam_coeffs,
    const float2* const RESTRICT pth,
    const float2* const RESTRICT pph,
    const float2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    float2* beam_theta,
    float2* beam_phi);

void harp_assemble_station_beam_cuda_float(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const float2* const __restrict__ beam_coeffs,
    const float2* const __restrict__ p_theta,
    const float2* const __restrict__ p_phi,
    const float2* const __restrict__ phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    float2* beam_theta,
    float2* beam_phi);

void harp_assemble_element_beams_float(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const float2* const RESTRICT coeffs,
    const float2* const RESTRICT pth,
    const float2* const RESTRICT pph,
    const float2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    float2* beam_theta,
    float2* beam_phi);

void harp_assemble_element_beams_cuda_float(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const float2* const RESTRICT coeffs,
    const float2* const RESTRICT pth,
    const float2* const RESTRICT pph,
    const float2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    float2* beam_theta,
    float2* beam_phi);

void harp_evaluate_phase_fac_double(
    const int num_dir,
    const int num_ant,
    const double freq,
    const double* const RESTRICT theta,
    const double* const RESTRICT phi,
    const double* const RESTRICT ant_x,
    const double* const RESTRICT ant_y,
    const double* const RESTRICT ant_z,
    double2* RESTRICT phase_fac);

void harp_evaluate_phase_fac_cuda_double(
    const int num_dir,
    const int num_ant,
    const double freq,
    const double* const RESTRICT theta,
    const double* const RESTRICT phi,
    const double* const RESTRICT ant_x,
    const double* const RESTRICT ant_y,
    const double* const RESTRICT ant_z,
    double2* RESTRICT phase_fac);

void harp_assemble_station_beam_double(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const double2* const RESTRICT beam_coeffs,
    const double2* const RESTRICT pth,
    const double2* const RESTRICT pph,
    const double2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    double2* beam_theta,
    double2* beam_phi);

void harp_assemble_station_beam_cuda_double(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const double2* const __restrict__ beam_coeffs,
    const double2* const __restrict__ p_theta,
    const double2* const __restrict__ p_phi,
    const double2* const __restrict__ phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    double2* beam_theta,
    double2* beam_phi);

void harp_assemble_element_beams_double(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const double2* const RESTRICT coeffs,
    const double2* const RESTRICT pth,
    const double2* const RESTRICT pph,
    const double2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    double2* beam_theta,
    double2* beam_phi);

void harp_assemble_element_beams_cuda_double(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const double2* const RESTRICT coeffs,
    const double2* const RESTRICT pth,
    const double2* const RESTRICT pph,
    const double2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    double2* beam_theta,
    double2* beam_phi);

#ifdef __cplusplus
}
#endif

#endif /* include guard */
