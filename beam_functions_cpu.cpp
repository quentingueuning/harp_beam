/* See the LICENSE file at the top-level directory of this distribution. */

#include <cmath>
#include <cstdlib>
#include <cstdint>
#include "vector_types.h"
#include "harp_beam.h"

#define C0 299792458
#define PI 3.141592653589793

/*
 * STEP 1
 */

void harp_evaluate_beam_coeffs_double(
    const int num_mbf,
    const int num_ant,
    const double2* const RESTRICT weights,
    const double2* const RESTRICT coeffs,
    double2* RESTRICT beam_coeffs)
{
    // initialization of beam_coeffs
    for (int i = 0; i < num_mbf * num_ant; i++)
    {
        beam_coeffs[i].x = beam_coeffs[i].y = 0.0;
    }

    // matrix-product, beam_coeffs = coeffs*weights
    for (int i_ant = 0; i_ant < num_ant; i_ant++)
    {
        for (int i = 0; i < num_mbf * num_ant; i++)
        {
            const int index = i_ant * num_ant * num_mbf + i;
            beam_coeffs[i].x += coeffs[index].x * weights[i_ant].x -
                    coeffs[index].y * weights[i_ant].y;
            beam_coeffs[i].y += coeffs[index].y * weights[i_ant].x +
                    coeffs[index].x * weights[i_ant].y;
        }
    }
}


/*
 * STEP 2
 */

static void harp_legendre_multi_double(
    const int max_order,
    const int num_dir,
    const double* const RESTRICT theta,
    double* RESTRICT poly)
{
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        const double th = theta[i_dir];
        double sin_theta = sin(th);
        double cos_theta = cos(th);

        for (int n = 1; n <= max_order; ++n)
        {
            // constant needed in the computation of pnn
            double c = 1.0;
            for (int i = 1; i <= n; i++)
            {
                c *= (1.0 - 1.0 / ((double)i * 2));
            }

            const int i_leg = max_order * (max_order + 1) * i_dir +
                    (n - 1) * (max_order + 1);

            if (sin_theta != 0.0)
            {
                const double cotan_theta = cos_theta / sin_theta;

                // computation of two last polynomials pnn and pnn-1
                poly[i_leg + n] = sqrt(c) * pow(-sin_theta, (double)n); // pnn
                poly[i_leg + n - 1] = -2.0 * poly[i_leg + n] *
                        cotan_theta * n / sqrt(2.0 * n); // pnn-1

                // three-term downwards recurrence relation
                for (int m = n - 2; m > -1; m--)
                {
                    poly[i_leg + m] = (-2.0 * poly[i_leg + m + 1] *
                            cotan_theta * (double)(m + 1) -
                            poly[i_leg + m + 2] * sqrt((double) (n + m + 2)
                            * (n - m - 1))) / sqrt((double) (n + m + 1) * (n - m));
                }
            }
            else
            {
                // pn0(1) = cos_theta.^n
                poly[i_leg + 0] = pow(cos_theta, (double)n);

                // all other polynomials are 0
                for (int m = 1; m <= n; m++)
                {
                    poly[i_leg + m] = 0.0;
                }
            }
        }
    }
}

static void harp_evaluate_exp_phi_m_double(
    const int max_order,
    const int num_dir,
    const double* const RESTRICT phi,
    double2* RESTRICT ee)
{
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        for (int m = -max_order; m <= max_order; m++)
        {
            const double phi_m = m * phi[i_dir];
            double sin_phi_m = sin(phi_m);
            double cos_phi_m = cos(phi_m);

            const int index = i_dir * (2 * max_order + 1 ) + (m + max_order);
            ee[index].x = cos_phi_m;
            ee[index].y = sin_phi_m;
        }
    }
}

static void harp_evaluate_smodes_dd_qq_double(
    const int max_order,
    const int num_dir,
    const double* const RESTRICT poly,
    const double2* const RESTRICT ee,
    double2* RESTRICT qq,
    double2* RESTRICT dd)
{
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        for (int n = 1; n <= max_order; n++)
        {
            // Normalization factor.
            const double Nf = 1.0 / sqrt(4.0 * PI * n * (n + 1.0) / (2.0 * n + 1.0));

            for (int m = -n; m <= n; m++)
            {
                // Loading pre-computed exp(j*m*phi)
                const double2 eem = ee[i_dir * (2 * max_order + 1) + (m + max_order)];

                // Computation of m * Pnm/sin_theta from precomputed
                // Legendre polynomials in array "poly"
                double m_pnmd;
                const int i_n1m = max_order * (max_order + 1) * i_dir + (n - 2) * (max_order + 1) + abs(m); // index of p(n-1,m)
                if (m == 0)
                {
                    m_pnmd = 0.0;
                }
                else if ((abs(m) + 1) > (n - 1)) // when pn-1m+1 = 0
                {
                    if (n == 1) // when p(0,1) = 1
                    {
                        m_pnmd = -1.0 / sqrt(2.0);
                    }
                    else
                    {
                        m_pnmd = -1.0 / 2.0 * sqrt((double)(n + abs(m) - 1) * (n + abs(m))) * poly[i_n1m-1];
                    }
                }
                else
                {
                    m_pnmd = -1.0 / 2.0 *  (sqrt((double)(n - abs(m) - 1) * (n - abs(m))) * poly[i_n1m + 1] + sqrt((double) (n + abs(m) - 1) * (n + abs(m))) * poly[i_n1m - 1]);
                }

                // Just flip sign for pnm with negative degree m.
                if (m < 0) m_pnmd *= -1.0;

                // computation of dpnm/d(cos_theta) * sin_theta
                double dpnm;
                const int i_nm = max_order * (max_order + 1) * i_dir + (n - 1) * (max_order + 1) + abs(m); // index of p(n,m)
                if (abs(m) == 0) // when p(n,m-1) = p(n,-1)
                {
                    dpnm = - sqrt((double) (n + abs(m) + 1) * (n - abs(m))) * poly[i_nm + 1];
                }
                else if (abs(m) == n) // when p(n,m+1) = 0
                {
                    dpnm = 1.0 / 2.0 * sqrt((double) (n + abs(m)) * (n - abs(m) + 1)) * poly[i_nm - 1];
                }
                else
                {
                    dpnm = 1.0 / 2.0 * (sqrt((double) (n + abs(m)) * (n - abs(m) + 1)) * poly[i_nm - 1] - sqrt((double) (n + abs(m) + 1) * (n - abs(m))) * poly[i_nm + 1]);
                }

                // compute and store value of qq and dd for spherical mode of index "i_smodes
                const int i_smode = max_order * (2 * max_order + 1) * i_dir + (n - 1) * (2 * max_order + 1) + (m + max_order);
                qq[i_smode].x = -eem.x * dpnm * Nf;
                qq[i_smode].y = -eem.y * dpnm * Nf;
                dd[i_smode].x = eem.x * m_pnmd  * Nf;
                dd[i_smode].y = eem.y * m_pnmd * Nf;
            }
        }
    }
}

void harp_precompute_smodes_mbf_double(
    const int max_order,
    const int num_dir,
    const double* const RESTRICT theta,
    const double* const RESTRICT phi,
    double* RESTRICT poly,
    double2* RESTRICT ee,
    double2* RESTRICT qq,
    double2* RESTRICT dd)
{
    harp_legendre_multi_double(max_order, num_dir, theta, poly);
    harp_evaluate_exp_phi_m_double(max_order, num_dir, phi, ee);
    harp_evaluate_smodes_dd_qq_double(max_order, num_dir, poly, ee, qq, dd);
}

void harp_reconstruct_smodes_mbf_double(
    const int max_order,
    const int num_dir,
    const int num_mbf,
    const double2* const RESTRICT alpha_te,
    const double2* const RESTRICT alpha_tm,
    const double2* const RESTRICT qq,
    const double2* const RESTRICT dd,
    double2* RESTRICT p_theta,
    double2* RESTRICT p_phi)
{
    // initialization of MBF patterns p_theta and p_phi
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
        {
            const int index = i_dir * num_mbf + i_mbf;
            p_theta[index].x = p_theta[index].y = 0.0;
            p_phi[index].x = p_phi[index].y = 0.0;
        }
    }

    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        for (int n = 1; n <= max_order; n++)
        {
            for (int m = -n; m <= n; m++)
            {
                // loading precomputed values of qq and dd for smode (n,m)
                const int i_smode = max_order * (2 * max_order + 1) * i_dir + (n - 1) * (2 * max_order + 1) + (m + max_order);
                const double2 qq_ = qq[i_smode];
                const double2 dd_ = dd[i_smode];

                for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
                {
                    // loading spherical-wave te-,tm- coefficients
                    // alpha_te, alpha_tm associated with the patterns
                    // of mbf "i_mbf"
                    const int i_alpha = ((n - 1) * (2 * max_order + 1) + m + n) * num_mbf + i_mbf;
                    const double2 a_te = alpha_te[i_alpha];
                    const double2 a_tm = alpha_tm[i_alpha];

                    // evaluation and storage of the theta and phi
                    // polarizations of the MBFs patterns using coefficients
                    // and dd,qq values
                    const int i_out = i_dir * num_mbf + i_mbf;
                    p_theta[i_out].x += a_tm.x * qq_.x - a_tm.y * qq_.y + a_te.x * dd_.y + a_te.y * dd_.x;
                    p_theta[i_out].y += a_tm.x * qq_.y + a_tm.y * qq_.x - a_te.x * dd_.x + a_te.y * dd_.y;
                    p_phi[i_out].x += a_te.x * qq_.x - a_te.y * qq_.y - a_tm.x * dd_.y - a_tm.y * dd_.x;
                    p_phi[i_out].y += a_te.x * qq_.y + a_te.y * qq_.x + a_tm.x * dd_.x - a_tm.y * dd_.y;
                }
            }
        }
    }
}

/*
 * STEP 3
 */

void harp_evaluate_phase_fac_double(
    const int num_dir,
    const int num_ant,
    const double freq,
    const double* const RESTRICT theta,
    const double* const RESTRICT phi,
    const double* const RESTRICT ant_x,
    const double* const RESTRICT ant_y,
    const double* const RESTRICT ant_z,
    double2* RESTRICT phase_fac)
{
    // wavenumber
    const double k0 = 2.0 * PI * freq / C0;

    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        // cartesian spectral coordinates
        const double kx = k0 * sin(theta[i_dir]) * cos(phi[i_dir]);
        const double ky = k0 * sin(theta[i_dir]) * sin(phi[i_dir]);
        const double kz = k0 * cos(theta[i_dir]);

        for (int i_ant = 0; i_ant < num_ant; i_ant++)
        {
            // exp(j (kx * xant + ky * yant + kz * zant))
            const int index = i_dir * num_ant + i_ant;
            const double phase =
                    kx * ant_x[i_ant] + ky * ant_y[i_ant] + kz * ant_z[i_ant];
            phase_fac[index].x = cos(phase);
            phase_fac[index].y = sin(phase);
        }
    }
}

void harp_assemble_station_beam_double(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const double2* const RESTRICT beam_coeffs,
    const double2* const RESTRICT p_theta,
    const double2* const RESTRICT p_phi,
    const double2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    double2* beam_theta,
    double2* beam_phi)
{
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        // Initialization of the local beam values
        double2 beam_theta_local = { 0.0, 0.0 };
        double2 beam_phi_local = { 0.0, 0.0 };

        for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
        {
            // initialization of the array factors
            double2 AF_local = { 0.0, 0.0 };

            // Computation of the array factors
            for (int i_ant = 0; i_ant < num_ant; i_ant++)
            {
                const int64_t i_coe = i_ant * num_mbf + i_mbf;
                const int i_ph = i_dir * num_ant + i_ant;
                AF_local.x += phase_fac[i_ph].x * beam_coeffs[i_coe].x -
                        phase_fac[i_ph].y * beam_coeffs[i_coe].y;
                AF_local.y += phase_fac[i_ph].x * beam_coeffs[i_coe].y +
                        phase_fac[i_ph].y * beam_coeffs[i_coe].x;
            }

            // beam_th = sum_i_mbf p_theta(i_mbf) * AF(i_mbf)
            const int i_p = i_dir * num_mbf + i_mbf;
            beam_theta_local.x += AF_local.x * p_theta[i_p].x - AF_local.y * p_theta[i_p].y;
            beam_theta_local.y += AF_local.x * p_theta[i_p].y + AF_local.y * p_theta[i_p].x;
            beam_phi_local.x += AF_local.x * p_phi[i_p].x - AF_local.y * p_phi[i_p].y;
            beam_phi_local.y += AF_local.x * p_phi[i_p].y + AF_local.y * p_phi[i_p].x;
        }
        beam_theta[stride * i_dir + beam_theta_offset] = beam_theta_local;
        beam_phi[stride * i_dir + beam_phi_offset] = beam_phi_local;
    }
}

void harp_assemble_element_beams_double(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const double2* const RESTRICT beam_coeffs,
    const double2* const RESTRICT p_theta,
    const double2* const RESTRICT p_phi,
    const double2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    double2* beam_theta,
    double2* beam_phi)
{
    for (int i_ant = 0; i_ant < num_ant; i_ant++)
    {
        for (int i_dir = 0; i_dir < num_dir; i_dir++)
        {
            // Initialization of the local beam values
            double2 beam_theta_local = { 0.0, 0.0 };
            double2 beam_phi_local = { 0.0, 0.0 };

            for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
            {
                // initialization of the array factors
                double2 AF_local = { 0.0, 0.0 };
                const int64_t shift = (int64_t)i_ant * (int64_t)num_mbf * (int64_t)num_ant;

                // Computation of the array factors
                for (int i_a = 0; i_a < num_ant; i_a++)
                {
                    const int64_t i_coe = shift + i_a * num_mbf + i_mbf;
                    const int i_ph = i_dir * num_ant + i_a;
                    AF_local.x += phase_fac[i_ph].x * beam_coeffs[i_coe].x -
                            phase_fac[i_ph].y * beam_coeffs[i_coe].y;
                    AF_local.y += phase_fac[i_ph].x * beam_coeffs[i_coe].y +
                            phase_fac[i_ph].y * beam_coeffs[i_coe].x;
                }

                // beam_th = sum_i_mbf p_theta(i_mbf) * AF(i_mbf)
                const int i_p = i_dir * num_mbf + i_mbf;
                beam_theta_local.x += AF_local.x * p_theta[i_p].x - AF_local.y * p_theta[i_p].y;
                beam_theta_local.y += AF_local.x * p_theta[i_p].y + AF_local.y * p_theta[i_p].x;
                beam_phi_local.x += AF_local.x * p_phi[i_p].x - AF_local.y * p_phi[i_p].y;
                beam_phi_local.y += AF_local.x * p_phi[i_p].y + AF_local.y * p_phi[i_p].x;
            }

            // Multiply by conjugate of phase factor to remove phase gradient.
            const double2 phase = phase_fac[i_dir * num_ant + i_ant];
            const double2 b_theta_tmp = beam_theta_local;
            const double2 b_phi_tmp = beam_phi_local;
            beam_theta_local.x = b_theta_tmp.x * phase.x + b_theta_tmp.y * phase.y;
            beam_theta_local.y = b_theta_tmp.y * phase.x - b_theta_tmp.x * phase.y;
            beam_phi_local.x = b_phi_tmp.x * phase.x + b_phi_tmp.y * phase.y;
            beam_phi_local.y = b_phi_tmp.y * phase.x - b_phi_tmp.x * phase.y;

            const int i_out = stride * (i_ant * num_dir + i_dir);
            beam_theta[i_out + beam_theta_offset] = beam_theta_local;
            beam_phi[i_out + beam_phi_offset] = beam_phi_local;
        }
    }
}

/*
 * STEP 1
 */

void harp_evaluate_beam_coeffs_float(
    const int num_mbf,
    const int num_ant,
    const float2* const RESTRICT weights,
    const float2* const RESTRICT coeffs,
    float2* RESTRICT beam_coeffs)
{
    // initialization of beam_coeffs
    for (int i = 0; i < num_mbf * num_ant; i++)
    {
        beam_coeffs[i].x = beam_coeffs[i].y = 0.0;
    }

    // matrix-product, beam_coeffs = coeffs*weights
    for (int i_ant = 0; i_ant < num_ant; i_ant++)
    {
        for (int i = 0; i < num_mbf * num_ant; i++)
        {
            const int index = i_ant * num_ant * num_mbf + i;
            beam_coeffs[i].x += coeffs[index].x * weights[i_ant].x -
                    coeffs[index].y * weights[i_ant].y;
            beam_coeffs[i].y += coeffs[index].y * weights[i_ant].x +
                    coeffs[index].x * weights[i_ant].y;
        }
    }
}


/*
 * STEP 2
 */

static void harp_legendre_multi_float(
    const int max_order,
    const int num_dir,
    const float* const RESTRICT theta,
    float* RESTRICT poly)
{
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        const float th = theta[i_dir];
        float sin_theta = sin(th);
        float cos_theta = cos(th);

        for (int n = 1; n <= max_order; ++n)
        {
            // constant needed in the computation of pnn
            float c = 1.0;
            for (int i = 1; i <= n; i++)
            {
                c *= (1.0 - 1.0 / ((float)i * 2));
            }

            const int i_leg = max_order * (max_order + 1) * i_dir +
                    (n - 1) * (max_order + 1);

            if (sin_theta != 0.0)
            {
                const float cotan_theta = cos_theta / sin_theta;

                // computation of two last polynomials pnn and pnn-1
                poly[i_leg + n] = sqrt(c) * pow(-sin_theta, (float)n); // pnn
                poly[i_leg + n - 1] = -2.0 * poly[i_leg + n] *
                        cotan_theta * n / sqrt(2.0 * n); // pnn-1

                // three-term downwards recurrence relation
                for (int m = n - 2; m > -1; m--)
                {
                    poly[i_leg + m] = (-2.0 * poly[i_leg + m + 1] *
                            cotan_theta * (float)(m + 1) -
                            poly[i_leg + m + 2] * sqrt((float) (n + m + 2)
                            * (n - m - 1))) / sqrt((float) (n + m + 1) * (n - m));
                }
            }
            else
            {
                // pn0(1) = cos_theta.^n
                poly[i_leg + 0] = pow(cos_theta, (float)n);

                // all other polynomials are 0
                for (int m = 1; m <= n; m++)
                {
                    poly[i_leg + m] = 0.0;
                }
            }
        }
    }
}

static void harp_evaluate_exp_phi_m_float(
    const int max_order,
    const int num_dir,
    const float* const RESTRICT phi,
    float2* RESTRICT ee)
{
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        for (int m = -max_order; m <= max_order; m++)
        {
            const float phi_m = m * phi[i_dir];
            float sin_phi_m = sin(phi_m);
            float cos_phi_m = cos(phi_m);

            const int index = i_dir * (2 * max_order + 1 ) + (m + max_order);
            ee[index].x = cos_phi_m;
            ee[index].y = sin_phi_m;
        }
    }
}

static void harp_evaluate_smodes_dd_qq_float(
    const int max_order,
    const int num_dir,
    const float* const RESTRICT poly,
    const float2* const RESTRICT ee,
    float2* RESTRICT qq,
    float2* RESTRICT dd)
{
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        for (int n = 1; n <= max_order; n++)
        {
            // Normalization factor.
            const float Nf = 1.0 / sqrt(4.0 * PI * n * (n + 1.0) / (2.0 * n + 1.0));

            for (int m = -n; m <= n; m++)
            {
                // Loading pre-computed exp(j*m*phi)
                const float2 eem = ee[i_dir * (2 * max_order + 1) + (m + max_order)];

                // Computation of m * Pnm/sin_theta from precomputed
                // Legendre polynomials in array "poly"
                float m_pnmd;
                const int i_n1m = max_order * (max_order + 1) * i_dir + (n - 2) * (max_order + 1) + abs(m); // index of p(n-1,m)
                if (m == 0)
                {
                    m_pnmd = 0.0;
                }
                else if ((abs(m) + 1) > (n - 1)) // when pn-1m+1 = 0
                {
                    if (n == 1) // when p(0,1) = 1
                    {
                        m_pnmd = -1.0 / sqrt(2.0);
                    }
                    else
                    {
                        m_pnmd = -1.0 / 2.0 * sqrt((float)(n + abs(m) - 1) * (n + abs(m))) * poly[i_n1m-1];
                    }
                }
                else
                {
                    m_pnmd = -1.0 / 2.0 *  (sqrt((float)(n - abs(m) - 1) * (n - abs(m))) * poly[i_n1m + 1] + sqrt((float) (n + abs(m) - 1) * (n + abs(m))) * poly[i_n1m - 1]);
                }

                // Just flip sign for pnm with negative degree m.
                if (m < 0) m_pnmd *= -1.0;

                // computation of dpnm/d(cos_theta) * sin_theta
                float dpnm;
                const int i_nm = max_order * (max_order + 1) * i_dir + (n - 1) * (max_order + 1) + abs(m); // index of p(n,m)
                if (abs(m) == 0) // when p(n,m-1) = p(n,-1)
                {
                    dpnm = - sqrt((float) (n + abs(m) + 1) * (n - abs(m))) * poly[i_nm + 1];
                }
                else if (abs(m) == n) // when p(n,m+1) = 0
                {
                    dpnm = 1.0 / 2.0 * sqrt((float) (n + abs(m)) * (n - abs(m) + 1)) * poly[i_nm - 1];
                }
                else
                {
                    dpnm = 1.0 / 2.0 * (sqrt((float) (n + abs(m)) * (n - abs(m) + 1)) * poly[i_nm - 1] - sqrt((float) (n + abs(m) + 1) * (n - abs(m))) * poly[i_nm + 1]);
                }

                // compute and store value of qq and dd for spherical mode of index "i_smodes
                const int i_smode = max_order * (2 * max_order + 1) * i_dir + (n - 1) * (2 * max_order + 1) + (m + max_order);
                qq[i_smode].x = -eem.x * dpnm * Nf;
                qq[i_smode].y = -eem.y * dpnm * Nf;
                dd[i_smode].x = eem.x * m_pnmd  * Nf;
                dd[i_smode].y = eem.y * m_pnmd * Nf;
            }
        }
    }
}

void harp_precompute_smodes_mbf_float(
    const int max_order,
    const int num_dir,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    float* RESTRICT poly,
    float2* RESTRICT ee,
    float2* RESTRICT qq,
    float2* RESTRICT dd)
{
    harp_legendre_multi_float(max_order, num_dir, theta, poly);
    harp_evaluate_exp_phi_m_float(max_order, num_dir, phi, ee);
    harp_evaluate_smodes_dd_qq_float(max_order, num_dir, poly, ee, qq, dd);
}

void harp_reconstruct_smodes_mbf_float(
    const int max_order,
    const int num_dir,
    const int num_mbf,
    const float2* const RESTRICT alpha_te,
    const float2* const RESTRICT alpha_tm,
    const float2* const RESTRICT qq,
    const float2* const RESTRICT dd,
    float2* RESTRICT p_theta,
    float2* RESTRICT p_phi)
{
    // initialization of MBF patterns p_theta and p_phi
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
        {
            const int index = i_dir * num_mbf + i_mbf;
            p_theta[index].x = p_theta[index].y = 0.0;
            p_phi[index].x = p_phi[index].y = 0.0;
        }
    }

    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        for (int n = 1; n <= max_order; n++)
        {
            for (int m = -n; m <= n; m++)
            {
                // loading precomputed values of qq and dd for smode (n,m)
                const int i_smode = max_order * (2 * max_order + 1) * i_dir + (n - 1) * (2 * max_order + 1) + (m + max_order);
                const float2 qq_ = qq[i_smode];
                const float2 dd_ = dd[i_smode];

                for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
                {
                    // loading spherical-wave te-,tm- coefficients
                    // alpha_te, alpha_tm associated with the patterns
                    // of mbf "i_mbf"
                    const int i_alpha = ((n - 1) * (2 * max_order + 1) + m + n) * num_mbf + i_mbf;
                    const float2 a_te = alpha_te[i_alpha];
                    const float2 a_tm = alpha_tm[i_alpha];

                    // evaluation and storage of the theta and phi
                    // polarizations of the MBFs patterns using coefficients
                    // and dd,qq values
                    const int i_out = i_dir * num_mbf + i_mbf;
                    p_theta[i_out].x += a_tm.x * qq_.x - a_tm.y * qq_.y + a_te.x * dd_.y + a_te.y * dd_.x;
                    p_theta[i_out].y += a_tm.x * qq_.y + a_tm.y * qq_.x - a_te.x * dd_.x + a_te.y * dd_.y;
                    p_phi[i_out].x += a_te.x * qq_.x - a_te.y * qq_.y - a_tm.x * dd_.y - a_tm.y * dd_.x;
                    p_phi[i_out].y += a_te.x * qq_.y + a_te.y * qq_.x + a_tm.x * dd_.x - a_tm.y * dd_.y;
                }
            }
        }
    }
}

/*
 * STEP 3
 */

void harp_evaluate_phase_fac_float(
    const int num_dir,
    const int num_ant,
    const float freq,
    const float* const RESTRICT theta,
    const float* const RESTRICT phi,
    const float* const RESTRICT ant_x,
    const float* const RESTRICT ant_y,
    const float* const RESTRICT ant_z,
    float2* RESTRICT phase_fac)
{
    // wavenumber
    const float k0 = 2.0 * PI * freq / C0;

    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        // cartesian spectral coordinates
        const float kx = k0 * sin(theta[i_dir]) * cos(phi[i_dir]);
        const float ky = k0 * sin(theta[i_dir]) * sin(phi[i_dir]);
        const float kz = k0 * cos(theta[i_dir]);

        for (int i_ant = 0; i_ant < num_ant; i_ant++)
        {
            // exp(j (kx * xant + ky * yant + kz * zant))
            const int index = i_dir * num_ant + i_ant;
            const float phase =
                    kx * ant_x[i_ant] + ky * ant_y[i_ant] + kz * ant_z[i_ant];
            phase_fac[index].x = cos(phase);
            phase_fac[index].y = sin(phase);
        }
    }
}

void harp_assemble_station_beam_float(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const float2* const RESTRICT beam_coeffs,
    const float2* const RESTRICT p_theta,
    const float2* const RESTRICT p_phi,
    const float2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    float2* beam_theta,
    float2* beam_phi)
{
    for (int i_dir = 0; i_dir < num_dir; i_dir++)
    {
        // Initialization of the local beam values
        float2 beam_theta_local = { 0.0, 0.0 };
        float2 beam_phi_local = { 0.0, 0.0 };

        for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
        {
            // initialization of the array factors
            float2 AF_local = { 0.0, 0.0 };
            
            // Computation of the array factors
            for (int i_ant = 0; i_ant < num_ant; i_ant++)
            {
                const int64_t i_coe = i_ant * num_mbf + i_mbf;
                const int i_ph = i_dir * num_ant + i_ant;
                AF_local.x += phase_fac[i_ph].x * beam_coeffs[i_coe].x -
                        phase_fac[i_ph].y * beam_coeffs[i_coe].y;
                AF_local.y += phase_fac[i_ph].x * beam_coeffs[i_coe].y +
                        phase_fac[i_ph].y * beam_coeffs[i_coe].x;
            }

            // beam_th = sum_i_mbf p_theta(i_mbf) * AF(i_mbf)
            const int i_p = i_dir * num_mbf + i_mbf;
            beam_theta_local.x += AF_local.x * p_theta[i_p].x - AF_local.y * p_theta[i_p].y;
            beam_theta_local.y += AF_local.x * p_theta[i_p].y + AF_local.y * p_theta[i_p].x;
            beam_phi_local.x += AF_local.x * p_phi[i_p].x - AF_local.y * p_phi[i_p].y;
            beam_phi_local.y += AF_local.x * p_phi[i_p].y + AF_local.y * p_phi[i_p].x;
        }
        beam_theta[stride * i_dir + beam_theta_offset] = beam_theta_local;
        beam_phi[stride * i_dir + beam_phi_offset] = beam_phi_local;
    }
}

void harp_assemble_element_beams_float(
    const int num_mbf,
    const int num_ant,
    const int num_dir,
    const float2* const RESTRICT beam_coeffs,
    const float2* const RESTRICT p_theta,
    const float2* const RESTRICT p_phi,
    const float2* const RESTRICT phase_fac,
    const int stride,
    const int beam_theta_offset,
    const int beam_phi_offset,
    float2* beam_theta,
    float2* beam_phi)
{
    for (int i_ant = 0; i_ant < num_ant; i_ant++)
    {
        for (int i_dir = 0; i_dir < num_dir; i_dir++)
        {
            // Initialization of the local beam values
            float2 beam_theta_local = { 0.0, 0.0 };
            float2 beam_phi_local = { 0.0, 0.0 };

            for (int i_mbf = 0; i_mbf < num_mbf; i_mbf++)
            {
                // initialization of the array factors
                float2 AF_local = { 0.0, 0.0 };
                const int64_t shift = (int64_t)i_ant * (int64_t)num_mbf * (int64_t)num_ant;

                // Computation of the array factors
                for (int i_a = 0; i_a < num_ant; i_a++)
                {
                    const int64_t i_coe = shift + i_a * num_mbf + i_mbf;
                    const int i_ph = i_dir * num_ant + i_a;
                    AF_local.x += phase_fac[i_ph].x * beam_coeffs[i_coe].x -
                            phase_fac[i_ph].y * beam_coeffs[i_coe].y;
                    AF_local.y += phase_fac[i_ph].x * beam_coeffs[i_coe].y +
                            phase_fac[i_ph].y * beam_coeffs[i_coe].x;
                }

                // beam_th = sum_i_mbf p_theta(i_mbf) * AF(i_mbf)
                const int i_p = i_dir * num_mbf + i_mbf;
                beam_theta_local.x += AF_local.x * p_theta[i_p].x - AF_local.y * p_theta[i_p].y;
                beam_theta_local.y += AF_local.x * p_theta[i_p].y + AF_local.y * p_theta[i_p].x;
                beam_phi_local.x += AF_local.x * p_phi[i_p].x - AF_local.y * p_phi[i_p].y;
                beam_phi_local.y += AF_local.x * p_phi[i_p].y + AF_local.y * p_phi[i_p].x;
            }

            // Multiply by conjugate of phase factor to remove phase gradient.
            const float2 phase = phase_fac[i_dir * num_ant + i_ant];
            const float2 b_theta_tmp = beam_theta_local;
            const float2 b_phi_tmp = beam_phi_local;
            beam_theta_local.x = b_theta_tmp.x * phase.x + b_theta_tmp.y * phase.y;
            beam_theta_local.y = b_theta_tmp.y * phase.x - b_theta_tmp.x * phase.y;
            beam_phi_local.x = b_phi_tmp.x * phase.x + b_phi_tmp.y * phase.y;
            beam_phi_local.y = b_phi_tmp.y * phase.x - b_phi_tmp.x * phase.y;

            const int i_out = stride * (i_ant * num_dir + i_dir);
            beam_theta[i_out + beam_theta_offset] = beam_theta_local;
            beam_phi[i_out + beam_phi_offset] = beam_phi_local;
        }
    }
}
